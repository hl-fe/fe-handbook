import autoprefixer from 'autoprefixer';
import postcssFlexbugsFixes from 'postcss-flexbugs-fixes';
import { defineConfig } from 'vite';

export default () => {
  return defineConfig({
    css: {
      postcss: {
        plugins: [
          // 前缀追加
          autoprefixer({
            overrideBrowserslist: [
              'Android 4.1',
              'iOS 7.1',
              'Chrome > 31',
              'ff > 31',
              '> 1%',
            ],
            grid: true,
          }),
          postcssFlexbugsFixes(),
        ],
      },
    },
  });
};
