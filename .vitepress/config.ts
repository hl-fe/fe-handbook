import { defineConfig } from "vitepress";
import path from "path";
import { mdPlugin } from "./../plugins/markdownIt";
const resolve = ( dir ) => path.join( __dirname, dir );
const IS_PROD = () => {
  return process.argv[process.argv.length - 1] === "build";
};

// https://vitepress.dev/reference/site-config
export default defineConfig({
  outDir: resolve( "../document" ),
  base: IS_PROD() ? "/handbook/" : "/",
  title: "前端码农",
  description: "前端码农手记",
  head: [
    ["link", { rel: "icon", type: "image/svg+xml", href: "images/logo.svg" }],
  ],
  lastUpdated: true,
  markdown: {
    // theme: "material-palenight",
    lineNumbers: false,
    config: ( md ) => mdPlugin( md ),
  },
  themeConfig: {
    lastUpdatedText: "上次更新",
    // editLink: {
    //   pattern: "https://gitee.com/javaweb-h5/fe-handbook",
    //   text: "在GitHub上编辑此页",
    // },
    logo: "images/logo.svg",
    nav: [
      { text: "主页", link: "/" },
      { text: "指南", link: "/pages/guide" },
      { text: "更新日志", link: "/pages/updateLog" },
    ],
    socialLinks: [
      // {
      //   icon: "github",
      //   link: "https://gitee.com/javaweb-h5/fe-handbook",
      // },
    ],
    sidebar: [
      {
        collapsed: false,
        items: [
          {
            text: "开发指南",
            link: "/pages/guide",
          },
        ],
      },
      {
        text: "JavaScript高级程序设计",
        collapsed: false,
        items: [
          {
            text: "0.前言",
            link: "/pages/advancedProgramming/index.md",
          },
          {
            text: "1.什么是JavaScript",
            link: "/pages/advancedProgramming/chapterOne.md",
          },
          {
            text: "2.HTML中的JavaScript",
            link: "/pages/advancedProgramming/chapterTwo.md",
          },
          {
            text: "3.语言基础",
            link: "/pages/advancedProgramming/chapterThree.md",
          },
        ],
      },
      { 
        text: "vue技术栈",
        collapsed: false,
        items: [
          {
            text: "vue2.0",
            link: "/pages/vue/vue2.0.md",
          },
          {
            text: "vue3.0",
            link: "/pages/vue/vue3.0.md",
          },
          {
            text: "vuex4.0",
            link: "/pages/vue/vuex4.0.md",
          },
        ],
      },
      {
        text: "react技术栈",
        collapsed: false,
        items: [
          {
            text: "react18",
            link: "/pages/react/react.md",
          },
          {
            text: "redux",
            link: "/pages/react/redux.md",
          },
        ],
      },
      {
        text: "nodejs技术栈",
        collapsed: false,
        items: [
          {
            text: "nodeJS",
            link: "/pages/nodeJS/index.md",
          },
          {
            text: "nest",
            link: "/pages/nodeJS/nest.md",
          },
        ],
      },
      {
        text: "flutter",
        collapsed: false,
        items: [
          {
            text: "flutter",
            link: "/pages/flutter/index.md",
          },
        ],
      },
      {
        text: "数据结构算法&设计模式",
        collapsed: false,
        items: [
          {
            text: "dataStructure",
            link: "/pages/dataStructure.md",
          },
          {
            text: "设计模式",
            link: "/pages/designPatterns.md",
          },
        ],
      },
      {
        text: "js基础进阶",
        collapsed: false,
        items: [
          {
            text: "数据类型",
            link: "/pages/jsBase/dataType.md",
          },
          {
            text: "数据类型转换",
            link: "/pages/jsBase/typeConversion.md",
          },
          {
            text: "原型与原型链",
            link: "/pages/jsBase/prototype.md",
          },
          {
            text: "闭包",
            link: "/pages/jsBase/scope.md",
          },
          {
            text: "this指针",
            link: "/pages/jsBase/this.md",
          },
          {
            text: "JS中的变量提升和函数提升",
            link: "/pages/jsBase/promotion.md",
          },
          {
            text: "异步&事件循环机制",
            link: "/pages/jsBase/eventLoop.md",
          },
          {
            text: "promise",
            link: "/pages/jsBase/promise.md",
          },
          {
            text: "cookie&local",
            link: "/pages/jsBase/cookieLocal.md",
          },
          {
            text: "arrayApi",
            link: "/pages/jsBase/arrayApi.md",
          },
          {
            text: "正则",
            link: "/pages/jsBase/regular.md",
          },
          {
            text: "coding",
            link: "/pages/jsBase/coding.md",
          },
          {
            text: "typescript",
            link: "/pages/jsBase/typescript.md",
          },
        ],
      },
      {
        text: "前端综合",
        collapsed: false,
        items: [
          {
            text: "前端八股文",
            link: "/pages/jsBase/eightLeggedEssay.md",
          },
          {
            text: "二进制",
            link: "/pages/jsBase/binary.md",
          },
          {
            text: "时区",
            link: "/pages/timeZone.md",
          },
          {
            text: "ssr",
            link: "/pages/ssr.md",
          },
          {
            text: "webpack",
            link: "/pages/webpack.md",
          },
          {
            text: "http",
            link: "/pages/http.md",
          },
          {
            text: "性能优化",
            link: "/pages/optimize.md",
          },
          {
            text: "xss-csrf",
            link: "/pages/xss-csrf.md",
          },
          {
            text: "linux",
            link: "/pages/linux.md",
          },
          {
            text: "nginx",
            link: "/pages/nginx.md",
          },
          {
            text: "git管理",
            link: "/pages/git.md",
          },
        ],
      },
      {
        text: "工作复盘",
        collapsed: false,
        items: [
          {
            text: "stripe支付体检优化",
            link: "/pages/workReview/stripe.md",
          },
          {
            text: "接口签名设计",
            link: "/pages/workReview/interfaceSignatureDesign.md",
          },
          {
            text: "redux数据持久化",
            link: "/pages/workReview/reduxpersistence.md",
          },
          {
            text: "vuex数据持久化",
            link: "/pages/workReview/vuexpersistence.md",
          },
          {
            text: "react插槽设计",
            link: "/pages/workReview/reactSolt.md",
          },
          {
            text: "大文件上传",
            link: "/pages/workReview/bigfileUpload.md",
          },
          {
            text: "音频录制",
            link: "/pages/workReview/audio.md",
          },
          {
            text: "虚拟列表",
            link: "/pages/workReview/virtuaLlist.md",
          },
          {
            text: "图片裁剪&压缩",
            link: "/pages/workReview/imageCroppingCompression.md",
          },
          {
            text: "axios封装",
            link: "/pages/workReview/axiosPackaging.md",
          },
          {
            text: "前端监控SDK",
            link: "/pages/workReview/monitor.md",
          },
          {
            text: "图片拖动验证",
            link: "/pages/workReview/ImagedragVerification.md",
          },
        ],
      },
      {
        text: "html/css",
        collapsed: false,
        items: [
          {
            text: "盒模型",
            link: "/pages/htmlCss/boxModel.md",
          },
          {
            text: "margin重叠",
            link: "/pages/htmlCss/margin.md",
          },
          {
            text: "bfc",
            link: "/pages/htmlCss/bfc.md",
          },
          {
            text: "圣杯布局&双飞翼布局",
            link: "/pages/htmlCss/layout.md",
          },
          {
            text: "flex画骰子",
            link: "/pages/htmlCss/dice.md",
          },
          {
            text: "position定位",
            link: "/pages/htmlCss/position.md",
          },
          {
            text: "对齐",
            link: "/pages/htmlCss/align.md",
          },
          {
            text: "rem-em-vw",
            link: "/pages/htmlCss/rem-em-vw.md",
          },
          {
            text: "grid布局",
            link: "/pages/htmlCss/grid.md",
          },
          {
            text: "htmlCss总结",
            link: "/pages/htmlCss/htmlCss.md",
          },
        ],
      },
    ],
  }
})
