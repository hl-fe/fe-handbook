import DefaultTheme from "vitepress/theme";
import { ElButton, ElTooltip, ElIcon, ElRow } from "element-plus";
import Demo from "./../../components/Demo.vue";
import "animate.css";
import "./../../styles/index.scss";
import "element-plus/dist/index.css";
export default {
  ...DefaultTheme,
  enhanceApp({ app }) {
    app.component("Demo", Demo);
    app.component(ElButton.name, ElButton);
    app.component(ElTooltip.name, ElTooltip);
    app.component(ElIcon.name, ElIcon);
    app.component(ElRow.name, ElRow);
  },
};
