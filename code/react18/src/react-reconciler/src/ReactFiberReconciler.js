import { FiberRootNode } from "./ReactFiber";
import { createHostRootFiber } from './ReactFiber';
import { initialUpdateQueue, createUpdate, enqueueUpdate } from './ReactFiberClassUpdateQueue';
import { scheduleUpdateOnFiber, requestUpdateLane, requestEventTime } from "./ReactFiberWorkLoop";
export function createContainer ( container ) { // div#root
  // FiberRootNode
  const root = new FiberRootNode( container );
  // 根Fiber
  const hostRootFiber = createHostRootFiber();
  //根容器的current指向当前的根fiber
  root.current = hostRootFiber;
  //根fiber的stateNode,也就是真实DOM节点指向FiberRootNode
  hostRootFiber.stateNode = root;
  // 初始化更新队列
  initialUpdateQueue( hostRootFiber );
  return root;
}

/**
 * 
 * @param {*} element 
 * @param {*} fiberRootNode 
 */
export function updateContainer ( element, fiberRootNode ) {
  //获取当前的根fiber
  const hostRootFiber = fiberRootNode.current;
  const eventTime = requestEventTime();
  // 获取当前的Lane优先级
  const lane = requestUpdateLane();
  //创建更新
  const update = createUpdate(lane);
  // 根fiber队列存储的是虚拟DOM
  update.payload = { element }
  // 插入fiber队列
  enqueueUpdate( hostRootFiber, update );
  // 准备调度
  scheduleUpdateOnFiber( hostRootFiber,lane );
}
