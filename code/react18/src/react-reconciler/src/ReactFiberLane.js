import {
  ImmediatePriority as ImmediateSchedulerPriority,
  UserBlockingPriority as UserBlockingSchedulerPriority,
  NormalPriority as NormalSchedulerPriority,
  IdlePriority as IdleSchedulerPriority,
} from "./Scheduler"
export const TotalLanes = 31;
export const NoLanes = 0b0000000000000000000000000000000;
export const NoLane = 0b0000000000000000000000000000000;
// 离散事件优先级 click onchange
export const SyncLane = 0b0000000000000000000000000000001; 
export const InputContinuousHydrationLane = 0b0000000000000000000000000000010;
// 连续事件的优先级 mousemove 
export const InputContinuousLane = 0b0000000000000000000000000000100;
export const DefaultHydrationLane = 0b0000000000000000000000000001000;
// 默认事件车道
export const DefaultLane = 0b0000000000000000000000000010000;
export const SelectiveHydrationLane = 0b0001000000000000000000000000000;
export const IdleHydrationLane = 0b0010000000000000000000000000000;
// //空闲事件优先级 
export const IdleLane = 0b0100000000000000000000000000000;
export const OffscreenLane = 0b1000000000000000000000000000000;
const NonIdleLanes = 0b0001111111111111111111111111111;
//没有时间戳
export const NoTimestamp = -1;

export function markRootUpdated(root, updateLane) {
  //pendingLanes指的此根上等待生效的lane
  root.pendingLanes |= updateLane;
}

// 删除已经完成lane
export function markRootFinished(root, lane) {
  root.pendingLanes &= ~lane;
}

//找到最右边的1 只能返回一个车道
export function getHighestPriorityLane(lanes) {
  return lanes & -lanes;
}

// 子集
export function isSubsetOfLanes(set, subset) {
  return (set & subset) === subset;
}
// 合并Lanes
export function mergeLanes(a, b) {
  return a | b;
}

/**
 * 从lane 转换到 调度器的优先级
 * @param lanes
 */
export function lanesToSchedulerPriority(lanes) {
  const lane = getHighestPriorityLane(lanes);

  if (lane === SyncLane) {
    return ImmediateSchedulerPriority;
  }
  if (lane === InputContinuousLane) {
    return UserBlockingSchedulerPriority;
  }
  if (lane === DefaultLane) {
    return NormalSchedulerPriority;
  }
  return IdleSchedulerPriority;
}