import {
  scheduleCallback as Scheduler_scheduleCallback,
  shouldYield,
  NormalPriority as NormalSchedulerPriority,
  cancelCallback as Scheduler_cancelCallback,
  now
} from "./Scheduler"
import { markUpdateLaneFromFiberToRoot } from "./ReactFiberConcurrentUpdates";
import { createWorkInProgress } from "./ReactFiber";
import { beginWork } from "./ReactFiberBeginWork";
import { completeWork } from "./ReactFiberCompleteWork";
import { NoFlags, MutationMask, Passive } from "./ReactFiberFlags";
import { getCurrentEventPriority } from 'react-dom-bindings/src/client/ReactDOMHostConfig'
import {
  commitMutationEffectsOnFiber,//执行DOM操作
  commitPassiveUnmountEffects,//执行destroy
  commitPassiveMountEffects,//执行create
} from './ReactFiberCommitWork';
import { getCurrentUpdatePriority } from './ReactEventPriorities'
import {
  NoLanes,
  markRootUpdated,
  getHighestPriorityLane, 
  SyncLane, 
  NoLane,
  markRootFinished,
  NoTimestamp, mergeLanes,
  lanesToSchedulerPriority
} from './ReactFiberLane';
import { flushSyncCallbacks, scheduleSyncCallback } from "./syncTaskQueue";


let workInProgress; // 正在执行的工作单元(Fiber)
let wipRootRenderLane = NoLane;
let rootDoesHavePassiveEffects = false;//此根节点上有没有useEffect类似的副作用
let rootWithPendingPassiveEffects = null;//具有useEffect副作用的根节点 FiberRootNode,根fiber.stateNode

const RootInComplete = 1; // 未执行完
const RootCompleted = 2; // 执行完

//保存当前的事件发生的时间
let currentEventTime = NoTimestamp;


// 调度
export function scheduleUpdateOnFiber ( hostRootFiber,lane ) {
  // 根据根Fiber,找到根节点，从根开始调度
  const root = markUpdateLaneFromFiberToRoot( hostRootFiber )
  markRootUpdated(root, lane);
  //确保调度执行root上的更新
  ensureRootIsScheduled( root );
}

/**
 * schedule调度阶段入口
 *  root 根节点
 */
function ensureRootIsScheduled ( root ) {
  // 获取一个最高的Lane
  let updateLane = getHighestPriorityLane(root.pendingLanes);
  // 先获取当前根上执行任务
  const existingCallback = root.callbackNode;
  // 如果没有Lane，取消调度
  if (updateLane === NoLane) {
    if (existingCallback !== null) {
      Scheduler_cancelCallback(existingCallback);
    }
    root.callbackNode = null;
    root.callbackPriority = NoLane;
    return;
  }
  // 当前的Lane优先级
  const curPriority = updateLane;
  // 之前的Lane优先级
  const prevPriority = root.callbackPriority;
  if (curPriority === prevPriority) {
    // 如果之前的优先级等于当前的优先级, 不需要重新的调度，scheduler会自动的获取performConcurrentWorkOnRoot的返回函数继续调度
    return;
  }
  // 当前产生了更高优先级调度，取消之前的调度
  if (existingCallback !== null) {
    Scheduler_cancelCallback(existingCallback);
  }
   // 新的回调任务
  let newCallbackNode = null;
  console.log(`${updateLane === SyncLane ? "微" : "宏"} 任务中调度，优先级：`, updateLane);
 
  if(updateLane === SyncLane){
   // 同步优先级，用微任务调度，并且不会走并发更新策略，无法中断
   scheduleSyncCallback(performSyncWorkOnRoot.bind(null, root));
   //微任务调度批处理
   queueMicrotask(flushSyncCallbacks);
   newCallbackNode = null
  }else{
    // 非同步优先级，用宏任务调度，并发更新策略，高优先级可以打断低优先级的更新
     // 将react lane 转换成 调度器的优先级
     const schedulerPriority = lanesToSchedulerPriority(updateLane);
     newCallbackNode = Scheduler_scheduleCallback(schedulerPriority,performConcurrentWorkOnRoot.bind(null, root));
  }
   //在根节点的执行的任务是newCallbackNode
   root.callbackNode = newCallbackNode
   root.callbackPriority = curPriority;
}



/**
 * 同步更新入口(render入口)
 */
function performSyncWorkOnRoot(root) {

  let nextLane = getHighestPriorityLane(root.pendingLanes);
  // 同步批处理中断的条件
  if (nextLane !== SyncLane) {
    // 其他比SyncLane 低的优先级
    // NoLane
    ensureRootIsScheduled(root);
    return;
  }
  let exitStatus = renderRoot(root, nextLane, true);

  // 执行完成
  if (exitStatus === RootCompleted) {
    const finishedWork = root.current.alternate; // 新的构建的fiber
    root.finishedWork = finishedWork;
    root.finishedLane = nextLane;
    wipRootRenderLane = NoLane;

    // commit阶段操作，渲染页面
    commitRoot(root);
  }
}

/**
 * 并发更新的render入口 -> scheduler时间切片执行的函数
 * @didTimeout: 调度器传入 -> 任务是否过期
 */
function performConcurrentWorkOnRoot(root,didTimeout) {
  const lane = getHighestPriorityLane(root.pendingLanes);
  const curCallbackNode = root.callbackNode;

  if (lane === NoLane) {
    return null;
  }

  // 同步任务，过期已经过期，需要同步执行
  const needSync = lane === SyncLane || didTimeout;

  // render阶段
  const exitStatus = renderRoot(root, lane, needSync);

  // 再次执行调度，用于判断之后root.callbackNode === curCallbackNode,
  // 因为如果并发过程中，优先级没有变，在执行调度后，由于curPriority === prevPriority，直接返回，导致curCallbackNode相等，继续调度
  // 如果有更高优先级的调度的话，本次调度直接返回null,停止调度
  ensureRootIsScheduled(root);

  // 中断
  if (exitStatus === RootInComplete) {
    // ensureRootIsScheduled中有更高的优先级插入进来, 停止之前的调度
    if (root.callbackNode !== curCallbackNode) {
      return null;
    }
    // 继续调度
    return performConcurrentWorkOnRoot.bind(null, root);
  }

  // 已经更新完
  if (exitStatus === RootCompleted) {
    const finishedWork = root.current.alternate;
    root.finishedWork = finishedWork;
    root.finishedLane = lane;
    wipRootRenderLane = NoLane;

    commitRoot(root);
  }
}

/**
 * 并发和同步更新的入口（render阶段）
 * @param root
 * @param lane
 * @param shouldTimeSlice 是否同步更新
 */
function renderRoot(root, lane, shouldTimeSlice) {
  // 由于并发更新会不断的执行，但是并不需要更新，所以我们需要判断优先级看看是否需要初始化
  // 如果wipRootRenderLane 不等于 当前更新的lane， 就需要重新初始化，从根部开始调度
  if (wipRootRenderLane !== lane) {
    // 初始化，将workInProgress 指向第一个fiberNode
    prepareFreshStack(root, lane);
  }

  do {
    try {
      shouldTimeSlice ? workLoopSync() : workLoopConcurrent();
      break;
    } catch (e) {
      workInProgress = null;
    }
  } while (true);

  // 中断执行(并发更新，还有工作单元)
  if (!shouldTimeSlice && workInProgress !== null) {
    return RootInComplete;
  }
  //render阶段执行完
  return RootCompleted;
}

function flushPassiveEffects() {
  if (rootWithPendingPassiveEffects !== null) {
    const root = rootWithPendingPassiveEffects;
    commitPassiveUnmountEffects(root.current);
    commitPassiveMountEffects(root, root.current);
  }
}

// 从根开始提交
function commitRoot ( root ) {
  const { finishedWork } = root;

  // Effect执行
  if ((finishedWork.subtreeFlags & Passive) !== NoFlags || (finishedWork.flags & Passive) !== NoFlags) {
    if (!rootDoesHavePassiveEffects) {
      rootDoesHavePassiveEffects = true;
      Scheduler_scheduleCallback(NormalSchedulerPriority,flushPassiveEffects);
    }
  }
  const lane = root.finishedLane;
  root.finishedWork = null;
  root.finishedLane = NoLane;
  markRootFinished(root, lane);
  const rootHasEffect = ( finishedWork.flags & MutationMask ) !== NoFlags;
  const subtreeHasEffects = ( finishedWork.subtreeFlags & MutationMask ) !== NoFlags;
  //如果自己的副作用或者子节点有副作用就进行提交DOM操作
  if ( subtreeHasEffects || rootHasEffect ) {
    commitMutationEffectsOnFiber( finishedWork, root );
    if (rootDoesHavePassiveEffects) {
      rootDoesHavePassiveEffects = false;
      rootWithPendingPassiveEffects = root;
    }
  }
  //等DOM变更后，就可以把让root的current指向新的fiber树
  root.current = finishedWork;
  //在提交之后，因为根上可能会有跳过的更新，所以需要重新再次调度
  ensureRootIsScheduled(root, now());
}

function prepareFreshStack ( fiberRootNode,lane ) {
  root.finishedLane = NoLane;
  root.finishedWork = null;
  workInProgress = createWorkInProgress( fiberRootNode.current, null );
  wipRootRenderLane = lane;
}

// 同步执行无法中断
function workLoopSync () {
  while ( workInProgress !== null ) {
    performUnitOfWork( workInProgress );
  }
}

// 并发更新，可以中断
function workLoopConcurrent() {
  //如果有下一个要构建的fiber并且时间片没有过期
  while (workInProgress !== null && !shouldYield()) {
    performUnitOfWork(workInProgress);
  }
}

// 执行一个工作单元
function performUnitOfWork ( unitOfWork ) {
  // 构建当前fiber的子fiber,返回子fiber
  const next = beginWork( unitOfWork );
  unitOfWork.memoizedProps = unitOfWork.pendingProps;
  if ( next === null ) {
    //如果没有子节点表示当前的fiber已经完成了
    completeUnitOfWork( unitOfWork );
  } else {
    // 下个工作单元
    workInProgress = next;
  }
}


function completeUnitOfWork ( unitOfWork ) {
  let completedWork = unitOfWork;
  const current = completedWork.alternate;
  do {
    //执行此fiber 的完成工作,如果是原生组件的话就是创建真实的DOM节点
    completeWork( current, completedWork );
    //如果有弟弟，就构建弟弟对应的fiber子链表
    const siblingFiber = completedWork.sibling;
    if ( siblingFiber !== null ) {
      workInProgress = siblingFiber;
      return;
    }
    //如果没有弟弟，说明这当前完成的就是父fiber的最后一个节点
    //也就是说一个父fiber,所有的子fiber全部完成了
    completedWork = completedWork.return
    workInProgress = completedWork
  } while ( completedWork !== null );
}

// 获取当前的Lane优先级
// 如果没有设置优先级，获取当前的事件优先级，转换成Lane优先级
export function requestUpdateLane() {
  const updateLane = getCurrentUpdatePriority();
  if (updateLane !== NoLanes) {
    return updateLane;
  }
  const eventLane = getCurrentEventPriority();
  return eventLane;
}

//请求当前的时间
export function requestEventTime() {
  currentEventTime = now()
  return currentEventTime;//performance.now()
}