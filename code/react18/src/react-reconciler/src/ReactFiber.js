import { HostComponent, HostRoot, IndeterminateComponent, HostText } from "./ReactWorkTags";

import { NoFlags } from "./ReactFiberFlags";
import { NoLanes,NoLane } from "./ReactFiberLane";
/**
 *
 * @param {*} tag fiber的类型 函数组件0  类组件1 根元素3 原生组件5 
 * @param {*} pendingProps 新属性，等待处理或者说生效的属性
 * @param {*} key 唯一标识
 */
export function FiberNode ( tag, pendingProps, key ) {
  this.tag = tag;
  this.key = key;
  this.type = null; // fiber类型，来自于 虚拟DOM节点的type  span div p 组件
  this.stateNode = null; // 真实的DOM

  this.return = null; //指向父节点
  this.child = null; //指向第一个子节点
  this.sibling = null; //指向弟弟

  //fiber哪来的？通过虚拟DOM节点创建，虚拟DOM会提供pendingProps用来创建fiber节点的属性
  this.pendingProps = pendingProps; //等待生效的属性
  this.memoizedProps = null; //已经生效的属性

  //每个fiber还会有自己的状态，每一种fiber 状态存的类型是不一样的
  //类组件对应的fiber 存的就是类的实例的状态,HostRoot存的就是要渲染的元素
  this.memoizedState = null;
  //每个fiber身上可能还有更新队列
  this.updateQueue = null;
  //副作用的标识，表示要针对此fiber节点进行何种操作
  this.flags = NoFlags; //自己的副作用
  //子节点对应的副使用标识
  this.subtreeFlags = NoFlags;
  //替身
  this.alternate = null;
  this.index = 0;
  this.deletions = null;
  this.lanes = NoLanes;
  this.childLanes = NoLanes;
  this.ref = null;
}

// 根据根创建的FiberRootNode
export function FiberRootNode ( containerInfo ) {
  this.containerInfo = containerInfo;//div#root
  //表示此根上有哪些赛道等待被处理
  this.pendingLanes = NoLanes;
  this.callbackNode = null;
  this.callbackPriority = NoLane;
  this.expiredLanes = NoLanes;

}

// 创建根Fiber
export function createHostRootFiber () {
  return createFiber( HostRoot, null, null );
}

// 创建Fiber
export function createFiber ( tag, pendingProps, key ) {
  return new FiberNode( tag, pendingProps, key );
}

/**
 * 基于老的fiberRootNode和新的属性创建新的fiberRootNode
 * @param {*} current 老fiberRootNode
 * @param {*} pendingProps 新属性
 */
export function createWorkInProgress ( current, pendingProps ) {
  let workInProgress = current.alternate; // 缓存的Fiber
  if ( workInProgress == null ) {
    // mount
    workInProgress = createFiber( current.tag, pendingProps, current.key );
    workInProgress.type = current.type;
    workInProgress.stateNode = current.stateNode;
    // 构建双缓存池
    workInProgress.alternate = current;
    current.alternate = workInProgress;
  } else {
    // update
    workInProgress.pendingProps = pendingProps;
    workInProgress.type = current.type;
    // 重置副作用
    workInProgress.flags = NoFlags;
    workInProgress.subtreeFlags = NoFlags;
  }
  workInProgress.child = current.child;
  workInProgress.memoizedProps = current.memoizedProps;
  workInProgress.memoizedState = current.memoizedState;
  workInProgress.updateQueue = current.updateQueue;
  workInProgress.sibling = current.sibling;
  workInProgress.index = current.index;
  return workInProgress
}




function createFiberFromTypeAndProps ( type, key, pendingProps ) {
  let tag = IndeterminateComponent; //
  //如果类型type是一字符串 span div ，说此此Fiber类型是一个原生组件
  if ( typeof type === "string" ) {
    tag = HostComponent;
  }
  const fiber = createFiber( tag, pendingProps, key );
  fiber.type = type;
  return fiber;
}

/**
 * 根据虚拟DOM创建Fiber节点
 * @param {*} element
 */
export function createFiberFromElement ( element ) {
  const { type, key, props: pendingProps } = element;
  return createFiberFromTypeAndProps( type, key, pendingProps );
}


/**
 * 根据虚拟DOM创建Fiber节点(文本)
 * @param {*} element
 */
export function createFiberFromText ( content ) {
  return createFiber( HostText, content, null );
}
