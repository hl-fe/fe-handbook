import { HostRoot, HostComponent, HostText, IndeterminateComponent, FunctionComponent } from "./ReactWorkTags";
import { processUpdateQueue } from "./ReactFiberClassUpdateQueue"
import { mountChildFibers, reconcileChildFibers } from "./ReactChildFiber";
import { renderWithHooks } from "./ReactFiberHooks";
import { shouldSetTextContent } from "react-dom-bindings/src/client/ReactDOMHostConfig";
// 构建子fiber
export function beginWork ( workInProgress,renderLane ) {
  switch ( workInProgress.tag ) {
    case HostRoot: // 处理根fiber
      return updateHostRoot( workInProgress,renderLane );
    case HostComponent: // 原生节点
      return updateHostComponent( workInProgress,renderLane );
    case IndeterminateComponent: // 函数、或者类组件
      return mountIndeterminateComponent( workInProgress, workInProgress.type,renderLane );
    case FunctionComponent: // 函数组件
      const Component = workInProgress.type;
      const nextProps = workInProgress.pendingProps;
      return updateFunctionComponent( workInProgress, Component, nextProps,renderLane );
    case HostText: // 文本没有子节点
      return null;
    default:
      return null;
  }
}

/**
 * 挂载函数组件
 * @param {*} workInProgress 新的fiber
 * @param {*} Component 组件类型，也就是函数组件的定义
 */
export function mountIndeterminateComponent ( workInProgress, Component ) {
  const props = workInProgress.pendingProps;
  const children = renderWithHooks( workInProgress, Component, props );
  workInProgress.tag = FunctionComponent;
  reconcileChildren( workInProgress, children );
  return workInProgress.child;
}

// 更新时的函数组件处理
export function updateFunctionComponent ( workInProgress, Component, nextProps ) {
  const nextChildren = renderWithHooks( workInProgress, Component, nextProps );
  reconcileChildren( workInProgress, nextChildren );
  return workInProgress.child;
}


// 处理根fiber,把虚拟节点插入在fiber队列中
function updateHostRoot ( workInProgress, renderLane ) {
  // 根据老状态和更新队列中的更新计算最新的状态
  // 根fiber队列中存储的是虚拟DOM
  processUpdateQueue( workInProgress, renderLane )
  const nextState = workInProgress.memoizedState;
  //nextChildren就是新的子虚拟DOM
  const nextChildren = nextState.element;
  // 构建子fiber
  reconcileChildren( workInProgress, nextChildren );
  return workInProgress.child;
}

// 根据当前的fiber，构建子fiber
function updateHostComponent ( workInProgress ) {
  // 根据element创建fiberNode
  const { type } = workInProgress;
  const nextProps = workInProgress.pendingProps;
  let nextChildren = nextProps.children;
  //判断当前虚拟DOM它的儿子是不是一个文本独生子
  const isDirectTextChild = shouldSetTextContent( type, nextProps );
  if ( isDirectTextChild ) {
    nextChildren = null;
  }
  reconcileChildren( workInProgress, nextChildren );
  return workInProgress.child;
}


function reconcileChildren ( workInProgress, children ) {
  // 当前的fiber是否缓存过
  const current = workInProgress.alternate;
  if ( current !== null ) {
    // 更新,需要diff
    workInProgress.child = reconcileChildFibers( workInProgress,
      current.child, // 老的子节点
      children )
  } else {
    // 首次渲染
    workInProgress.child = mountChildFibers( workInProgress, null, children )
  }
}