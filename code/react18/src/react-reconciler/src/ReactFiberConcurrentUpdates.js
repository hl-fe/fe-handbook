import { HostRoot } from "./ReactWorkTags";

// 找根节点
export function markUpdateLaneFromFiberToRoot ( hostRootFiber ) {
  let node = hostRootFiber;//当前fiber
  let parent = hostRootFiber.return;//当前fiber父fiber
  while ( parent !== null ) {
    node = parent
    parent = parent.return
  }
  //一直找到parent为null
  if ( node.tag === HostRoot ) {
    return node.stateNode;
  }
  return null;
};