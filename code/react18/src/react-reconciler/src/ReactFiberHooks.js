import ReactSharedInternals from "shared/ReactSharedInternals";
import { createUpdate, enqueueUpdate } from "./ReactFiberClassUpdateQueue";
import { scheduleUpdateOnFiber } from './ReactFiberWorkLoop';
import { Passive as PassiveEffect } from './ReactFiberFlags'
import {  HasEffect as HookHasEffect,Passive as HookPassive} from './ReactHookEffectTags';
import { requestUpdateLane } from "./ReactFiberWorkLoop";
import { NoLane } from "./ReactFiberLane";
const { ReactCurrentDispatcher } = ReactSharedInternals;
let currentlyRenderingFiber = null; // Function组件对应的fiber，hooks会临时挂载
let workInProgressHook = null; // 当前的hooks
let currentHook = null; // 函数组件更新时，正在执行的hook
// hook是批量的更新，多次触发只会更新一次
// 当在调用hook改变状态时，其实是调用的，hook dispatch函数时，会把状态追加到队列中
// 开始执行任务调度，从根节点开始渲染
// 函数在更新时，队列中的状态不会合并，取最后一次的更新状态


//useState其实就是一个内置了reducer的useReducer
function baseStateReducer ( state, action ) {
  return typeof action === 'function' ? action( state ) : action;
}

/**
 * 渲染函数组件
 * @param {*} workInProgress 新fiber
 * @param {*} Component 组件定义
 * @param {*} props 组件属性
 * @returns 虚拟DOM或者说React元素
 */
export function renderWithHooks ( workInProgress, Component, props ) {
  currentlyRenderingFiber = workInProgress; // Function组件对应的fiber
  // 重置hooks,effect更新队列
  //函数组件状态存的hooks的链表
  workInProgress.memoizedState = null;
   //函数组件更新队列里存的effect
  workInProgress.updateQueue = null;
  const current = workInProgress.alternate; // 老的fiber
  //如果有老的fiber,并且有老的hook链表
  if ( current ) {
    // 更新
    ReactCurrentDispatcher.current = HooksDispatcherOnUpdate;
  } else {
    // 首次
    ReactCurrentDispatcher.current = HooksDispatcherOnMount;
  }
  const children = Component( props );

  // 重置
  currentlyRenderingFiber = null;
  workInProgressHook = null;
  currentHook = null;
  return children;
}

// 函数组件首次渲染Mount hooks
// 1、函数组件首次渲染时，把所有的hook挂载fiber节点上，形成单向链表
// 2、每一个hook都有自己的dispatch函数，状态更新队列
// 3、当在函数组件调用hook dispatch函数时，会把状态追加到队列中
const HooksDispatcherOnMount = {
  useState: mountState,
  useEffect: mountEffect
};

// 函数组件更新
// 1、函数组件更新时，会从老的fiber节点上，获取hook
// 2、合并当前hook队列的状态，返回一个新的值
const HooksDispatcherOnUpdate = {
  useState: updateState,
  useEffect: updateEffect,
};


function mountState ( initialState ) {
  if ( currentlyRenderingFiber === null ) {
    // 必须在函数组件中使用hooks
    throw 'mountState时currentlyRenderingFiber不存在'
  }
  // 构建hook链表，返回当前的hook
  const hook = mountWorkInprogressHook();
  // hook的初始化状态
  hook.memoizedState = hook.baseState = initialState;
  // 初始hook化队列，一个hook,可以多次dispatch，每次存储在hook队列
  // 函数重新渲染时，计算最新的状态
  const updateQueue = {
    lastRenderedState: initialState,//上一个state
    updateQueue: {
      shared: {
        pending: null
      }
    }
  };
  hook.queue = updateQueue
  hook.dispatch = dispatchSetState.bind( null, currentlyRenderingFiber, updateQueue )
  return [ hook.memoizedState, hook.dispatch ]
}


function updateState () {
  if ( currentlyRenderingFiber === null ) {
    // 必须在函数组件中使用hooks
    throw 'mountState时currentlyRenderingFiber不存在'
  }
  
  // 获取当前执行的hook
  //获取新的hook
  const hook = updateWorkInProgressHook();
  //获取新的hook的更新队列
  const queue = hook.queue.updateQueue.shared;
  //获取老的hook
  const current = currentHook;
  //获取将要生效的更新队列
  const pendingQueue = queue.pending;
  //初始化一个新的状态，取值为当前的状态
  let newState = current.memoizedState;
  // 合并计算新的状态
  if ( pendingQueue !== null ) {
    queue.pending = null;
    const firstUpdate = pendingQueue.next;
    // 第一个队列
    let update = firstUpdate;
    do {
      newState = baseStateReducer( newState, update.payload.action );
      update = update.next;
    } while ( update !== null && update !== firstUpdate )
  }
  hook.memoizedState = newState;

  return [ hook.memoizedState, hook.dispatch ];
}



function dispatchSetState ( fiber, updateQueue, action ) {
  const lane = requestUpdateLane(); // 每一个更新设置一个lane(优先级）
  //创建更新
  const update = createUpdate(lane) 
  let { lastRenderedState } = updateQueue
  // 计算新的值
  let nextRenderedState = baseStateReducer( lastRenderedState, action );
  // 如果值没有发生改变，不需要渲染
  if ( Object.is( lastRenderedState, nextRenderedState ) ) {
    return;
  }
  update.payload = { action }
  // 插入hook队列中,渲染的时候合并状态
  enqueueUpdate( updateQueue, update )
  // 渲染
  scheduleUpdateOnFiber( fiber,lane );
}



function mountEffect(create, deps){
  return mountEffectImpl(PassiveEffect, HookPassive, create, deps);
}

function mountEffectImpl(fiberFlags, hookFlags, create, deps){
  const hook = mountWorkInprogressHook();
  const nextDeps = deps === undefined ? null : deps;
  //给当前的函数组件fiber添加flags
  currentlyRenderingFiber.flags |= fiberFlags;
  hook.memoizedState = pushEffect(HookHasEffect | hookFlags, create, undefined, nextDeps);
}

/**
 * 添加effect链表
 * @param {*} tag effect的标签
 * @param {*} create 创建方法
 * @param {*} destroy 销毁方法
 * @param {*} deps 依赖数组
 */
function pushEffect(tag, create, destroy, deps) {
  const effect = {
    tag,
    create,
    destroy,
    deps,
    next: null
  }
  // Effect存储在updateQueue循环链接中
  let componentUpdateQueue = currentlyRenderingFiber.updateQueue;
  if (componentUpdateQueue === null) {
    componentUpdateQueue = createFunctionComponentUpdateQueue();
    currentlyRenderingFiber.updateQueue = componentUpdateQueue;
    componentUpdateQueue.lastEffect = effect.next = effect;
  } else {
    const lastEffect = componentUpdateQueue.lastEffect;
    if (lastEffect === null) {
      componentUpdateQueue.lastEffect = effect.next = effect;
    } else {
      const firstEffect = lastEffect.next;
      lastEffect.next = effect;
      effect.next = firstEffect;
      componentUpdateQueue.lastEffect = effect;
    }
  }
  return effect;
}

function createFunctionComponentUpdateQueue() {
  return {
    lastEffect: null
  }
}

function updateEffect(create, deps) {
  return updateEffectImpl(PassiveEffect, HookPassive, create, deps);
}
function updateEffectImpl(fiberFlags, hookFlags, create, deps) {
  const hook = updateWorkInProgressHook();
  const nextDeps = deps === undefined ? null : deps;
  let destroy;
  //上一个老hook
  if (currentHook !== null) {
    //获取此useEffect这个Hook上老的effect对象 create deps destroy
    const prevEffect = currentHook.memoizedState;
    destroy = prevEffect.destroy;
    if (nextDeps !== null) {
      const prevDeps = prevEffect.deps;
      // 用新数组和老数组进行对比，如果一样的话
      if (areHookInputsEqual(nextDeps, prevDeps)) {
        //不管要不要重新执行，都需要把新的effect组成完整的循环链表放到fiber.updateQueue中
        hook.memoizedState = pushEffect(hookFlags, create, destroy, nextDeps);
        return;
      }
    }
  }
  //如果要执行的话需要修改fiber的flags
  currentlyRenderingFiber.flags |= fiberFlags;
  //如果要执行的话 添加HookHasEffect flag
  //刚才有同学问 Passive还需HookHasEffect,因为不是每个Passive都会执行的
  hook.memoizedState = pushEffect(HookHasEffect | hookFlags, create, destroy, nextDeps);
}

function areHookInputsEqual(nextDeps, prevDeps) {
  if (prevDeps === null)
    return null;
  for (let i = 0; i < prevDeps.length && i < nextDeps.length; i++) {
    if (Object.is(nextDeps[i], prevDeps[i])) {
      continue;
    }
    return false;
  }
  return true;
}

/**
 * 挂载构建中的hook
 * */
function mountWorkInprogressHook () {
  const hook = {
    memoizedState: null,//hook的状态
    queue: null,//存放本hook的更新队列 queue.pending=update的循环链表
    next: null, //指向下一个hook,一个函数里可以会有多个hook,它们会组成一个单向链表
    baseState: null,//第一跳过的更新前的状态
    baseQueue: null//跳过的更新的链表
  };
 
  if ( workInProgressHook === null ) {
    // 首次渲染memoizedState链表头
    currentlyRenderingFiber.memoizedState = workInProgressHook = hook;
  } else {
    // 构建hook链表
    workInProgressHook = workInProgressHook.next = hook;
  }
  return workInProgressHook

}

/**
 * 构建新的hooks
 */
function updateWorkInProgressHook () {
  //获取将要构建的新的hook的老hook
  if ( currentHook === null ) {
    // 获取老Fiber的hook
    const current = currentlyRenderingFiber.alternate;
    currentHook = current.memoizedState;
  } else {
    currentHook = currentHook.next;
  }
  //根据老hook创建新hook
  const newHook = {
    memoizedState: currentHook.memoizedState,
    queue: currentHook.queue,
    dispatch: currentHook.dispatch,
    baseState: currentHook.baseState,
    baseQueue: currentHook.baseQueue,
    next: null
  }
  if ( workInProgressHook === null ) {
    currentlyRenderingFiber.memoizedState = workInProgressHook = newHook;
  } else {
    workInProgressHook = workInProgressHook.next = newHook;
  }
  return workInProgressHook;
}