import { getEventPriority } from '../client/ReactDOMHostConfig';
import { getCurrentUpdatePriority, setCurrentUpdatePriority } from 'react-reconciler/src/ReactEventPriorities';

// 支持的事件类型
const allNativeEvents = [ 'click' ];
const elementEventPropsKey = '__props';

const listeningMarker = `_reactListening` + Math.random().toString( 36 ).slice( 2 );

// 事件映射
function getEventCallbackNameFromtEventType ( eventType ) {
  return {
    click: [ 'onClickCapture', 'onClick' ]
  }[ eventType ]
}



// 收集从目标元素到HostRoot之间所有目标回调函数
const collectPaths = (
  targetElement,
  container,
  eventType
) => {
  const paths = {
    capture: [],
    bubble: []
  };
  // 收集事件回调是冒泡的顺序
  while ( targetElement && targetElement !== container ) {
    // div.__props = {onClickCapture:fn,onClick:fn....}
    const eventProps = targetElement[ elementEventPropsKey ];
    if ( eventProps ) {
      const callbackNameList = getEventCallbackNameFromtEventType( eventType );
      // 取出映射的事件
      if ( callbackNameList ) {
        callbackNameList.forEach( ( callbackName, i ) => {
          // react事件
          const eventCallback = eventProps[ callbackName ];
          if ( eventCallback ) {
            if ( i === 0 ) {
              // 由于捕获的执行顺序是从上往下，所以是反向收集
              // 反向插入捕获阶段的事件回调
              paths.capture.unshift( eventCallback );
            } else {
              // 由于冒泡的执行顺序是从下往上，所以是正向收集
              // 正向插入冒泡阶段的事件回调
              paths.bubble.push( eventCallback );
            }
          }
        } );
      }
    }
    targetElement = targetElement.parentNode;
  }
  return paths;
};

const dispatchEvent = ( rootContainerElement, eventType, e ) => {
  const targetElement = e.target; // 事件源

  if ( targetElement === null ) {
    console.error( '事件不存在target', e );
    return;
  }
  // 从当前的事件源向上查找所有的，扑捉、冒泡事件，用于模拟原生的事件
  const { capture, bubble } = collectPaths(
    targetElement,
    rootContainerElement,
    eventType
  );
  // 合成原生事件
  const se = createSyntheticEvent( e );
  triggerEventFlow( capture, se )
  // 如果事件被阻止不需要在执行
  if ( !se.__stopPropagation ) {
    triggerEventFlow( bubble, se );
  }

};

export function listenToAllSupportedEvents ( rootContainerElement ) {
  //监听根容器，也就是div#root只监听一次
  if ( !rootContainerElement[ listeningMarker ] ) {
    rootContainerElement[ listeningMarker ] = true;
    allNativeEvents.forEach( ( eventType ) => {
      // 注册代理事件
      rootContainerElement.addEventListener( eventType, ( e ) => {
        dispatchEvent( rootContainerElement, eventType, e );
      } );
    } );
  }
}


function createSyntheticEvent ( e ) {
  const syntheticEvent = e;
  syntheticEvent.__stopPropagation = false;
  // 阻止事件传递
  const originStopPropagation = e.stopPropagation;

  syntheticEvent.stopPropagation = () => {
    syntheticEvent.__stopPropagation = true;
    if ( originStopPropagation ) {
      originStopPropagation();
    }
  };

  return syntheticEvent;
}


// 执行react事件
const triggerEventFlow = ( paths, se ) => {
  for ( let i = 0; i < paths.length; i++ ) {
    const callback = paths[ i ];
    // 为事件添加事件优先级
    let previousPriority = getCurrentUpdatePriority
    try {
      setCurrentUpdatePriority(getEventPriority(se.type));
      callback.call( null, se );
    } finally {
      setCurrentUpdatePriority(previousPriority);
    }
    if ( se.__stopPropagation ) {
      break;
    }
  }
};


// 将支持的事件回调保存在DOM中
export const updateEventProps = (
  node,
  props
) => {
  node[ elementEventPropsKey ] = node[ elementEventPropsKey ] || {}
  allNativeEvents.forEach( ( eventType ) => {
    // 获取支持的事件类型
    const callbackNameList = getEventCallbackNameFromtEventType( eventType );
    if ( !callbackNameList ) {
      return;
    }
    // 事件映射
    callbackNameList.forEach( ( callbackName ) => {
      if ( Object.hasOwnProperty.call( props, callbackName ) ) {
        node[ elementEventPropsKey ][ callbackName ] = props[ callbackName ];
      }
    } );
  } );
  return node
};