import { setInitialProperties, diffProperties, updateProperties } from './ReactDOMComponent';
import { updateEventProps } from '../events/DOMPluginEventSystem';
import { ContinuousEventPriority, DefaultEventPriority, DiscreteEventPriority, getCurrentUpdatePriority, setCurrentUpdatePriority } from 'react-reconciler/src/ReactEventPriorities';

export function createTextInstance ( content ) {
  return document.createTextNode( content );
}
export function createInstance ( type, props ) {
  const domElement = document.createElement( type );
  // 存储事件
  return updateEventProps( domElement, props );
}

export function appendInitialChild ( parent, child ) {
  parent.appendChild( child );
}
export function finalizeInitialChildren ( domElement, type, props ) {
  setInitialProperties( domElement, type, props );
}
export function appendChild ( parentInstance, child ) {
  parentInstance.appendChild( child );
}
/**
 * 
 * @param {*} parentInstance 父DOM节点
 * @param {*} child 子DOM节点
 * @param {*} beforeChild 插入到谁的前面，它也是一个DOM节点
 */
export function insertBefore ( parentInstance, child, beforeChild ) {
  parentInstance.insertBefore( child, beforeChild );
}

export function prepareUpdate ( domElement, type, oldProps, newProps ) {
  return diffProperties( domElement, type, oldProps, newProps );
}

export function commitUpdate ( domElement, updatePayload, type, oldProps, newProps ) {
  updateProperties( domElement, updatePayload, type, oldProps, newProps );
  // 存储事件
  updateEventProps( domElement, newProps );
}


export function removeChild ( parentInstance, child ) {
  parentInstance.removeChild( child );
}

export function shouldSetTextContent ( type, props ) {
  return typeof props.children === "string" || typeof props.children === "number";
}


export function getCurrentEventPriority() {
  const currentEvent = window.event;
  if (currentEvent === undefined) {
    return DefaultEventPriority;
  }
  return getEventPriority(currentEvent.type);
}


/**
 * 获取事件优先级
 * @param {*} domEventName 事件的名称  click
 */
export function getEventPriority(domEventName) {
  switch (domEventName) {
      case "click":
      case "keydown":
      case "keyup":
      return DiscreteEventPriority;
    case "scroll":  
    case 'drag':
      return ContinuousEventPriority;
    default:
      return DefaultEventPriority;
  }
}
