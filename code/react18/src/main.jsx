import * as React from "react";
import { createRoot } from "react-dom/client";

function FunctionComponent() {
  const [number, setNumber] = React.useState(0);
  React.useEffect(() => {
    console.log("useEffect1");
    return () => {
      console.log("destroy useEffect1");
    };
  });
  React.useEffect(() => {
    console.log("useEffect2");
    return () => {
      console.log("destroy useEffect2");
    };
  });
  React.useEffect(() => {
    console.log("useEffect3");
    return () => {
      console.log("destroy useEffect3");
    };
  });
  return (
    <div
      onClick={() => {
        setNumber(number +1);
       }}
     >
       {number}
     </div>
   );
}
let element = <FunctionComponent />;
// 创建根节点、根Fibe，让根节点的current指向根Fibe，根Fibe的stateNode指向根节点
const root = createRoot(document.getElementById("root"));
// 调用根节点的render函数，把虚拟DOM转换成真实的DOM,渲染到容器中
root.render(element);