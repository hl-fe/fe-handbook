// 无优先级 
export const NoPriority = 0;
// 立刻执行优先级(等待时间-1ms) 局长
export const ImmediatePriority = 1; // 
//用户阻塞操作优先级 用户点击 ，用户输入(等待时间250ms)  副局长
export const UserBlockingPriority = 2;
// 正常优先级(等待时间5000ms) 科长
export const NormalPriority = 3;
// 低优先级(等待时间10000ms) 科员
export const LowPriority = 4;
// 空闲优先级(没有过期时间) 临时工
export const IdlePriority = 5;