import {
  createContainer,
  updateContainer
} from 'react-reconciler/src/ReactFiberReconciler';

import { listenToAllSupportedEvents } from 'react-dom-bindings/src/events/DOMPluginEventSystem';

function ReactDOMRoot ( internalRoot ) {
  // 根节点创建的FiberRootNode
  this._internalRoot = internalRoot;
}

ReactDOMRoot.prototype.render = function ( children ) {
  const root = this._internalRoot; // FiberRootNode
  root.containerInfo.innerHTML = '';
  updateContainer( children, root );
}

export function createRoot ( container ) {// div#root
  // 初始化容器
  const root = createContainer( container );
  // 合成事件
  listenToAllSupportedEvents( container );
  return new ReactDOMRoot( root );
}