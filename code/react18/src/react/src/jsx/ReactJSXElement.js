import hasOwnProperty from 'shared/hasOwnProperty';
import { REACT_ELEMENT_TYPE } from 'shared/ReactSymbols';

const RESERVED_PROPS = {
  key: true,
  ref: true,
  __self: true,
  __source: true
}
function hasValidKey ( config ) {
  return config.key !== undefined;
}
function hasValidRef ( config ) {
  return config.ref !== undefined;
}
/**
 * babel在编译的时候会将jsx语法转换成浏览器能够识别的js，并且生成ReactElement(虚拟DOM)
 * 在React17以前，babel是调用的React.createElement,并且页面需要引入
 * 在React17之后，babel是调用的(react/jsx-runtimejsx)函数，并且页面无需引入
 * 
 * @param {*} type 类组件 函数组件 原生的标签div span
 * @param {*} config 
 */
export function jsxDEV ( type, config ) {
  const props = {};//属性对象
  let key = null;
  let ref = null;
  if ( hasValidKey( config ) ) {
    key = config.key;
  }
  if ( hasValidRef( config ) ) {
    ref = config.ref;
  }

  for ( let propName in config ) {
    if ( hasOwnProperty.call( config, propName )
      && !RESERVED_PROPS.hasOwnProperty( propName )
    ) {
      props[ propName ] = config[ propName ]
    }
  }

  return ReactElement( type, key, ref, props );
}



// 虚拟节点
function ReactElement ( type, key, ref, props ) {
  return {
    $$typeof: REACT_ELEMENT_TYPE, // React元素标识
    type, // 类组件 函数组件 虚拟节点 原生的标签div span
    key, // 唯一标识
    ref, // 真实的dom节点
    props // 属性 children, style, id
  }
}

