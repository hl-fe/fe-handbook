import { forEachValue } from "../utils";
export default class Module {
  constructor ( rawModule ) {
    this._raw = rawModule; // 原始模块
    this.state = rawModule.state; // 状态
    this._children = {}; // 子模块
    this.namespaced = rawModule.namespaced; // 自己是否有命名空间
  }

  // 添加子模块
  addChild ( key, module ) {
    this._children[ key ] = module;
  }

  // 查询子模块
  getChild ( key ) {
    return this._children[ key ];
  }

  // 处理getters
  forEachGetter ( fn ) {
    if ( this._raw.getters ) {
      forEachValue( this._raw.getters, fn );
    }
  }

  // 处理Mutation
  forEachMutation ( fn ) {
    if ( this._raw.mutations ) {
      forEachValue( this._raw.mutations, fn );
    }
  }
  // 处理Action
  forEachAction ( fn ) {
    if ( this._raw.actions ) {
      forEachValue( this._raw.actions, fn );
    }
  }


  // 遍历子模块
  forEachChild ( fn ) {
    forEachValue( this._children, fn );
  }


}