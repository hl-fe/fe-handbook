import Module from "./module";
import { forEachValue } from "../utils";

export default class ModuleCollection {
  constructor ( rootModule ) {
    this.root = null;
    this.register( rootModule, [] );
  }
  // 递归格式化数据
  register ( rootModule, path ) {
    let module = new Module( rootModule )
    if ( path.length === 0 ) {
      // 根模块
      this.root = module
    } else {
      // 非根模块，需要把子模块安装在父模块_children中
      // [aCount].slice( 0, -1 ) = [] 
      // [aCount,cCount].slice( 0, -1 ) = [aCount]
      const parent = path.slice( 0, -1 ).reduce( ( module, moduleKey ) => {
        return module.getChild( moduleKey )
      }, this.root )
      // 子模块安装在父模块_children对象中
      parent.addChild( path[ path.length - 1 ], module );
    }

    // 处理子模块
    if ( rootModule.modules ) {
      forEachValue( rootModule.modules, ( key, rawChildModule ) => {
        this.register( rawChildModule, path.concat( key ) )
      } )
    }
  }

  // 拼接模块命名空间 aCount/cCount/
  getNamespaced ( path ) {
    let module = this.root
    return path.reduce( ( preKey, key ) => {
      // 如果子模块有namespaced拼接key
      module = module.getChild( key )
      return `${ preKey }${ module.namespaced ? `${ key }/` : "" }`
    }, '' )
  }
}