export const storeKey = "store";
import { inject } from "vue";
export function useStore ( injectKey = null ) {
  return inject( injectKey !== null ? injectKey : storeKey );
}
