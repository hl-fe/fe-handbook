export function forEachValue ( map, callback ) {
  for ( const key in map ) {
    callback( key, map[ key ] )
  }
}
export function isPromise ( val ) {
  return val && typeof val.then === "function";
}
