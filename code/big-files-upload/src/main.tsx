import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import Record from './Record.tsx'
import './index.css'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <Record/>
    <hr />
    <App />
  </React.StrictMode>,
)
