import { Toast, Button, Space } from 'antd-mobile'
import axios from 'axios'
import { useEffect, useRef, useState } from 'react'

interface InstanceRecord {
  stopRecord: () => void
  startRecord: () => void
  uploadAudio: () => void
  auditionAudio: () => void
}

function Record () {
  const [instanceRecord, setInstanceRecord] = useState<InstanceRecord>()
  const audioRef = useRef<HTMLAudioElement>(null)
  const [audioStream, setAudioStream] = useState<Blob[]>([])
  const [audioUrl, setAudioUrl] = useState<string>('')
  const [isRecordedAudio, setRecordedAudio] = useState<boolean>(false)
  const getInstanceRecord = (setAudioStream: React.Dispatch<React.SetStateAction<Blob[]>>, setAudioUrl: React.Dispatch<React.SetStateAction<string>>) => {
    return new Promise<InstanceRecord>((resolve, reject) => {
      navigator.mediaDevices.getUserMedia({ audio: true }).then((stream) => {
        const mediaRecorder = new MediaRecorder(stream)
        let chunks: Blob[] = []
        const startRecord = () => {
          console.log('开始录制音频')
          chunks = []
          mediaRecorder.start()
          mediaRecorder.addEventListener('dataavailable', (event: BlobEvent) => {
            chunks.push(event.data)

          })
          mediaRecorder.addEventListener('stop', () => {
            console.log('音频录制结束')
            setAudioStream(chunks)
          })
        }
        const stopRecord = () => {
          mediaRecorder.stop()
        }

        const uploadAudio = () => {
          console.log('音频上传')
          const blob = new Blob(chunks, { type: 'audio/ogg; codecs=opus' })
          const formData = new FormData()
          formData.append('file', blob)
          axios({
            method: 'post',
            url: `http://localhost:3000/upload/record`,
            data: formData,
            headers: {
              'Content-Type': 'multipart/form-data'
            },
          }).then(res => {
            setAudioUrl(res.data.url)
          })
        }

        const auditionAudio = () => {
          console.log('播放音频')
          const blob = new Blob(chunks, { type: 'audio/ogg; codecs=opus' })
          const audio = new Audio(URL.createObjectURL(blob))
          audio.play()
        }

        resolve({
          stopRecord,
          startRecord,
          uploadAudio,
          auditionAudio
        })
      }).catch((err: Error) => {
        reject(err)
      })
    })
  }

  // 播放
  const playAudio = () => {
    audioRef.current?.play();
    audioRef.current?.addEventListener('timeupdate', function () {
      if (this.currentTime === this.duration) {
        this.pause();
        this.currentTime = 0;
      }
    });
  }

  // 停止播放
  const pauseAudio = () => {
    audioRef.current?.pause();
  }

  const handleMouseDown = () => {
    instanceRecord?.startRecord()
    setRecordedAudio(true)
    document.addEventListener('mouseup', handleMouseUp);
  };

  const handleMouseUp = () => {
    instanceRecord?.stopRecord()
    setRecordedAudio(false)
    document.removeEventListener('mouseup', handleMouseUp);
  };

  useEffect(() => {
    getInstanceRecord(setAudioStream, setAudioUrl).then((instanceRecord) => {
      setInstanceRecord(instanceRecord)
    }).catch(() => {
      Toast.show('异常请重试')
    })
    return () => {
      document.removeEventListener('mouseup', handleMouseUp);
    };
  }, []);



  return (
    <>
      {
        audioUrl && <audio controls ref={audioRef}>
          <source src={audioUrl} type="audio/ogg"></source>
        </audio>
      }

      <Space block>
        <Button block type='submit' color='primary' size='large' onMouseDown={handleMouseDown}>
          {isRecordedAudio ? '正在录音' : '录音'}
          
        </Button>
        {audioStream.length > 0 && [
          <Button block type='submit' color='primary' size='large' onClick={() => instanceRecord?.uploadAudio()} key={'uploadAudio'}>
            上传录音
          </Button>, <Button block type='submit' color='primary' size='large' onClick={instanceRecord?.auditionAudio} key={'auditionAudio'}>
            试听
          </Button>]}
        {
          audioUrl && [<Button block type='submit' color='primary' size='large' onClick={playAudio} key={'playAudio'}>
            播放
          </Button>,
          <Button block type='submit' color='primary' size='large' onClick={pauseAudio} key={'pauseAudio'}>
            停止播放
          </Button>]
        }

      </Space>
    </>
  )
}

export default Record


