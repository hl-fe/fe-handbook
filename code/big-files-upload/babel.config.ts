const antdMobile  = {
  presets: ['@babel/preset-react'],
  plugins: [
    ['import', {
      libraryName: 'antd-mobile',
      style: true
    }]
  ]
}


export default antdMobile