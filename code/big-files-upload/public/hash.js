self.importScripts('./spark-md5.js');

self.onmessage = async (event) => {
  let { blobs } = event.data;
  const spark = new self.SparkMD5.ArrayBuffer();
  let percent = 0;
  let perSize = 100 / blobs.length; // 计算每一个chunks的百分比
  let buffers = await Promise.all(blobs.map((blob) => new Promise(function (resolve) {
    const reader = new FileReader();
    reader.readAsArrayBuffer(blob);
    reader.onload = function (event) {
      percent += perSize;
      self.postMessage({ percent: Number(percent.toFixed(2)) });
      resolve(event.target.result);
    }
  })));
  buffers.forEach(buffer => spark.append(buffer));
  self.postMessage({ percent: 100, hash: spark.end() });
  self.close();
}
