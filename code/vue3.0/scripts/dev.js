const execa = require( "execa" );

// 读取packages下文件，剔除非文件夹类型的
const target = "runtime-core";

build( target );
build( 'runtime-dom' );
// 对我们目标进行依次打包 ，并行打包
async function build ( target ) {
  // rollup  -c --environment TARGET:shated
  await execa( "rollup", [ "-cw", "--environment", `TARGET:${ target }` ], {
    stdio: "inherit", // 当子进程打包的信息共享给父进程
  } );
}
