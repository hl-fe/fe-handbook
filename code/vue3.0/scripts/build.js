const fs = require("fs");
const path = require("path");
const execa = require("execa");

const packagesDir = path.join(__dirname, "../packages");
const resolve = (p = "") => path.resolve(packagesDir, p);

// 读取packages下文件，剔除非文件夹类型的
const targets = fs
  .readdirSync(resolve())
  .filter((fileName) => fs.statSync(resolve(fileName)).isDirectory());

function runParallel(targets) {
  let res = [];
  for (const target of targets) {
    res.push(build(target));
  }
  return Promise.all(res);
}

// 对我们目标进行依次打包 ，并行打包
async function build(target) {
  // rollup  -c --environment TARGET:shated
  await execa("rollup", ["-c", "--environment", `TARGET:${target}`], {
    stdio: "inherit", // 当子进程打包的信息共享给父进程
  });
}

runParallel(targets);
