export const isObject = ( value ) => typeof value == "object" && value !== null;
export const hasChanged = ( oldValue, newValue ) => oldValue !== newValue;
export let isArray = Array.isArray;
export const isIntegerKey = ( key ) => parseInt( key ) + "" === key;
export const isString = ( value ) => typeof value === "string";
export const isFunction = ( value ) => typeof value === "function";
export const hasOwn = ( target, key ) =>
  Object.prototype.hasOwnProperty.call( target, key );
export const extend = Object.assign;
export * from "./shapeFlag";
