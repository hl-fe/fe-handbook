import { hasChanged, hasOwn, isArray, isIntegerKey, isObject } from "@vue/shared"
import { track, trigger } from "./effect"
import { TrackOpTypes, TriggerOrTypes } from "./operators"
import { reactive, ReactiveFlags, reactiveMap, readonly, readonlyMap, shallowReactiveMap, shallowReadonlyMap } from "./reactive"

function createGetter( isReadonly = false, shallow = false ) {
  return function get( target, key, receiver ) {
    if ( key === ReactiveFlags.IS_REACTIVE ) { // 是否已经是响应式
      return !isReadonly
    } else if ( key === ReactiveFlags.IS_READONLY ) { // 是否只读
      return isReadonly
    } else if (
      key === ReactiveFlags.RAW &&
      receiver === ( isReadonly ? shallow ? shallowReadonlyMap : readonlyMap : shallow ? shallowReactiveMap : reactiveMap
      ).get( target )
    ) {
      // 看是否缓存过，缓存过直接返回即可，toRaw调用的也是此方法
      return target
    }
    // 取代理对象的值
    const res = Reflect.get( target, key, receiver )

    if ( !isReadonly ) { // 不是只读的 就收集依赖
      console.log( JSON.stringify( target ), key, '收集依赖' )
      track( target, TrackOpTypes.GET, key )
    }

    // 如果是浅的只会在取值的时候，收集最外层的对象
    // 当最外层的数据发生变化后，可以触发视图更新
    // 对象里的对象发生变化，无法触发视图更新
    if ( shallow ) {
      return res
    }

    // vue3 针对的是对象来进行劫持，不用改写原来的对象，如果是嵌套，当取值的时候才会代理
    // vue2 针对的是属性劫持，改写了原来对象，一上来就递归的
    // vue3 可以对不存在的属性进行获取，也会走get方法，Proxy支持数组
    if ( isObject( res ) ) {
      // 懒递归 当我们取值的时候才去做递归代理，如果不取默认值代理一层
      return isReadonly ? readonly( res ) : reactive( res )
    }
    return res
  }
}


function createSetter( shallow = false ) {
  return function set( target, key, value, receiver ) {
    // 获取老值
    let oldValue = target[key]

    /**
     * fix:数组push
     * 数组在push的时候，会触发2次get拦截，第二次是无意义的，需要屏蔽掉
     * 第一次会给新的索引位置,新增一个值
     * 第二次改变数组的length的长度(会被屏蔽掉)
     */
    // 判断：是新增还是修改
    const hadKey = isArray( target ) && isIntegerKey( key )
      ? Number( key ) < target.length // 数组push会触发2次get,需要屏蔽到
      : hasOwn( target, key )
    const result = Reflect.set( target, key, value, receiver )
    if ( !hadKey ) {
      // 新增
      trigger( target, TriggerOrTypes.ADD, key, value, oldValue );
    } else if ( hasChanged( oldValue, value ) ) {
      // 修改
      trigger( target, TriggerOrTypes.UPDATE, key, value, oldValue );
    }
    return result
  }
}

// 只读无法修改数据
const readonlySet = {
  set( target, key ) {
    console.warn( `cannot set ${ JSON.stringify( target ) } on  key ${ key } falied` );
  },
};

// 深度
let reactiveGet = createGetter( false, false )
let reactiveSet = createSetter( false )
let reactiveHandlers = {
  get: reactiveGet,
  set: reactiveSet
}

// 浅的，非只读
let shallowReactiveGet = createGetter( false, true )
let shallowReactiveSet = createSetter( true )
let shallowReactiveHandlers = {
  get: shallowReactiveGet,
  set: shallowReactiveSet
}

// 只读，非浅的
let readonlyGet = createGetter( true, false )
let readonlyHandlers = {
  get: readonlyGet,
  set: readonlySet.set // 只读无法修改数据
}

// 浅的、只读
let shallowReadonlyGet = createGetter( true, true )
let shallowReadonlyHandlers = {
  get: shallowReadonlyGet,
  set: readonlySet.set // 只读无法修改数据
}


export {
  reactiveHandlers,
  shallowReactiveHandlers,
  readonlyHandlers,
  shallowReadonlyHandlers
}