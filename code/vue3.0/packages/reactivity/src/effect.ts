import { isArray, isIntegerKey } from "@vue/shared";
import { TriggerOrTypes } from "./operators";

export function effect( fn, options: any = {} ) {
  let effect = createReactiveEffect( fn, options )
  if ( !options.lazy ) {
    effect()
  }
  return effect
}

// effect发生多层嵌套时，要保证当前effect能够正确的收集

// effect1(() => {
//   state.name; activeEffect = effect1
//   effect2(() => {
//     state.name; activeEffect = effect2 
//   });
//   state.name; activeEffect = effect1
// });
let id = 0;
export let activeEffect;
const effectStack = [];
function createReactiveEffect( fn, options ) {

  let effect: any = function reactiveEffect() {
    if ( !effectStack.includes( effect ) ) { // 防止自己调用自己发生死循环
      try {
        effectStack.push( effect )
        activeEffect = effect
        return fn()
      } finally {
        effectStack.pop()
        activeEffect = effectStack[effectStack.length - 1]
      }
    }

  }

  effect.id = id++; // effect id，用于去重
  effect.__isEffect = true;
  effect.options = options;
  effect.deps = []; // 那些属性依赖了effect
  return effect;
}

/**
 * 响应式取值时，触发依赖收集effect
 * @param target 代理对象
 * @param type 类型
 * @param key 属性
 */
// {
//   '{ name:hl,age: 18,base:{add:sh} }': {
//      name: 'new Set( effect )',
//      age: 'new Set( effect )',
//      base: 'new Set( effect )',
//   },
//   '{ add: sh }': { sh: 'new Set( effect )' }
// }
const targetMap = new WeakMap()
export function track( target, type, key ) {
  if ( !activeEffect ) {
    // 用户只是取了值，而且这个值不是在effect中使用的 ，什么都不用收集
    return;
  }
  let depsMap = targetMap.get( target );
  // 代理对象是否收集过
  if ( !depsMap ) {
    targetMap.set( target, ( depsMap = new Map() ) );
  }
  let dep = depsMap.get( key );
  // 代理对象属性是否收集过
  if ( !dep ) {
    depsMap.set( key, ( dep = new Set() ) );
  }
  // 如果一个属性进行多次取值，需要去重
  if ( !dep.has( activeEffect ) ) {
    // 让属性去收集Effect
    dep.add( activeEffect );
  }
}


/**
 * 响应式发生改变时，通知effect更新
 * @param target 
 * @param type 
 * @param key 
 * @param newValue 
 * @param oldValue 
 */
export function trigger( target, type, key, newValue, oldValue?) {
  let depsMap = targetMap.get( target )
  if ( !depsMap ) {
    return;
  }
  const effectsSet = new Set();
  let effectsHandler = ( effects ) => {
    // 如果同时有多个 依赖的effect是同一个 还用set做了一个过滤
    if ( effects ) {
      effects.forEach( ( effect ) => {
        effectsSet.add( effect );
      } );
    }
  };
  if ( isArray( target ) ) {
    if ( isIntegerKey( key ) ) {

      switch ( type ) {
        case TriggerOrTypes.ADD:
          // 当数组在push操作的时候，会触发get操作，给(新的索引)位置,新增一个值
          // 因为索引是新加的，没有依赖收集，所以需要通过数组的length触发effect更新视图
          effectsHandler( depsMap.get( "length" ) );
      }
    } else if ( key === 'length' ) {
      let arrLength = newValue
      /**
       * key: 数组收集的属性
       * 1、如果模版中直接通过索引取值，只会触发当前的索引收集effect
       * 2、当数组的length发生改变，数组中收集的索引比当前的length大，都需要出发effect更新视图
       */

      depsMap.forEach( ( effect, key ) => {
        if ( key > arrLength || key === 'length' ) {
          effectsHandler( effect )
        }
      } )
    }

  } else {
    effectsHandler( depsMap.get( key ) )
  }

  effectsSet.forEach( ( effect: any ) => {
    // 是否执行自定义的函数
    if ( effect.options.schedular ) {
      effect.options.schedular( effect );
    } else {
      effect();
    }
  } );
}