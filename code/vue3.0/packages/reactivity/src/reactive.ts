
import { isObject } from '@vue/shared'
import {
  reactiveHandlers,
  shallowReactiveHandlers,
  readonlyHandlers,
  shallowReadonlyHandlers
} from "./baseHandlers";

export const enum ReactiveFlags {
  SKIP = '__v_skip',              //无需响应的对象
  IS_REACTIVE = '__v_isReactive',  //响应式对象
  IS_READONLY = '__v_isReadonly',  //只读数据
  RAW = '__v_raw',      //取原始对象
}

export const reactiveMap = new WeakMap()
export const shallowReactiveMap = new WeakMap()
export const readonlyMap = new WeakMap()
export const shallowReadonlyMap = new WeakMap()

/**
 * 
 * @param target 代理的目标对象
 * @param isReadonly 是否只读
 * @param baseHandlers 代理get、set
 * @param proxyMap 缓存的代理对象
 * @returns 
 */
function createReactiveObject(
  target,
  isReadonly,
  baseHandlers,
  proxyMap,
) {
  if ( !isObject( target ) ) { // 不是对象就直接跳过
    return target
  }

  // 如果已经被代理过了，直接返回
  // reactive(reactive(obj))
  if (
    target[ReactiveFlags.RAW] &&
    !( isReadonly && target[ReactiveFlags.IS_REACTIVE] )
  ) {
    return target
  }
  // 如果某一个对象已经被代理过，下次在进行代理，直接取缓存的结果
  // let obj = { name: "hulei" };
  // let state = reactive( obj );
  // reactive( obj );
  const existingProxy = proxyMap.get( target ) // 看目标是否被代理过
  // 如果代理过，直接返回代理过的对象
  if ( existingProxy ) {
    return existingProxy
  }

  // 创建代理对象
  let proxy = new Proxy( target, baseHandlers )
  proxyMap.set( target, proxy ) // 存到缓存中
  return proxy

}

// 深度代理(只要有取值操作，就会触发依赖收集)
// 在取值的时候，如果是对象会继续进行代理
export let reactive = ( target ) => {
  return createReactiveObject( target, false, reactiveHandlers, reactiveMap )
}

// 浅代理(只有最外层的对象取值时，才会触发依赖收集)
// 只会代理最外层
export let shallowReactive = ( target ) => {
  return createReactiveObject( target, false, shallowReactiveHandlers, shallowReactiveMap )
}

// 只读(在取值的时候，不能触发依赖收集，属性不能修改)
export let readonly = ( target ) => {
  return createReactiveObject( target, true, readonlyHandlers, readonlyMap )
}

// 浅的，只读(在取值的时候，不能触发依赖收集，属性不能修改)
// 只会代理最外层
export let shallowReadonly = ( target ) => {
  return createReactiveObject( target, true, shallowReadonlyHandlers, shallowReadonlyMap )
}

