import { isArray } from "@vue/shared";

class ObjectRefImpl {
  public __v_isRef = true;

  constructor( public target, public key ) { }

  get value() {
    return this.target[this.key]
  }

  set value( value ) {
    this.target[this.key] = value
  }
}

function toRef( target, key ) {
  return new ObjectRefImpl( target, key )
}

function toRefs( target ) {
  let res = isArray( target ) ? new Array( target.length ) : {};
  for ( const key in target ) {
    res[key] = toRef( target, key );
  }
  return res;
}


export {
  toRef,
  toRefs
}