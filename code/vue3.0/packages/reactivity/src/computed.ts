import { isObject } from "@vue/shared";
import { effect, track, trigger } from "./effect";
import { TrackOpTypes, TriggerOrTypes } from "./operators";

class ComputedRefImpl {
  public _value;
  public effect;
  public _dirty = true;
  constructor( public getter, public setter ) {
    this.effect = effect( getter, {
      lazy: true,// 在取值的时候，才执行getter
      schedular: () => {
        // 1、计算属性响应式数据发生改变后，取消缓存
        this._dirty = true
        // 2、通知渲染的effect更新视图
        trigger( this, TriggerOrTypes.UPDATE, "value", this._value );
      }
    } )
  }

  get value() {
    // 是否被缓存
    if ( this._dirty ) {
      // 用户在取值的时候，会通过effect调用计算属性的getter方法，让响应式数据去收集计算属性effect
      this._value = this.effect()
      // 缓存值
      this._dirty = false
    }
    // 计算属性收集渲染的effect
    track( this, TrackOpTypes.GET, "value" );
    return this._value
  }

  set value( newValue ) {
    this.setter( newValue )
  }
}

function computed( handler ) {
  let getter;
  let setter;
  if ( typeof handler === 'function' ) {
    getter = handler
    setter = () => {
      console.warn( `computed not set` );
    }
  } else if ( isObject( handler ) ) {
    getter = handler.set
    setter = handler.get
  } else {
    return console.warn( `computed It must be a function or an object` );
  }
  return new ComputedRefImpl( getter, setter );
}


export {
  computed
}