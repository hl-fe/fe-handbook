import { hasChanged, isObject } from "@vue/shared";
import { track, trigger } from "./effect";
import { TrackOpTypes, TriggerOrTypes } from "./operators";
import { reactive } from "./reactive";

const convert = ( v ) => ( isObject( v ) ? reactive( v ) : v );
class RefImpl {
  public _value; // 原始的值
  public __v_isRef = true; // 表示他是一个ref
  constructor( public rawValue, shallow ) {
    // 如果不是浅的，并且是对象，走reactive代理拿到结果
    this._value = shallow ? this.rawValue : convert( this.rawValue )
  }

  get value() {
    track( this, TrackOpTypes.GET, "value" );
    return this._value
  }

  set value( newValue ) {
    if ( hasChanged( this.rawValue, newValue ) ) {
      this.rawValue = newValue;
      this._value = newValue;
      trigger( this, TriggerOrTypes.UPDATE, "value", newValue, this.rawValue );
    }
  }
}



let createRef = ( value, shallow ) => {
  return new RefImpl( value, shallow );
}

function shallowRef( value ) {
  return createRef( value, true );
}

// 可以把普通值变成响应式数据
function ref( value ) {
  return createRef( value, false );
}

export {
  ref,
  shallowRef
}