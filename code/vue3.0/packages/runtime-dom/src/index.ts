import { extend } from "@vue/shared";
import { nodeOps } from "./nodeOps";
import { patchProp } from "./patchProp";
import { createRenderer } from "@vue/runtime-core";

const rendererOptions = extend( { patchProp }, nodeOps );


export const createApp = ( rootComponent, rootProps = null ) => {
  let app = createRenderer( rendererOptions ).createApp( rootComponent, rootProps )
  let { mount } = app;
  app.mount = ( container ) => {
    container = document.querySelector( container );
    container.innerHTML = "";
    mount( container );
  }
  return app

};


export * from "@vue/runtime-core";