export const patchStyle = (el, prev, next) => {
  const style = el.style; //获取样式
  if (next == null) {
    el.removeAttribute("style");
  } else {
    if (prev) {
      // 遍历老的,如果老的有，新的没有剔除
      for (const key in prev) {
        if (!next[key]) {
          style[key] = "";
        }
      }
    }
    if (next) {
      // 加新的样式
      for (const key in next) {
        style[key] = next[key];
      }
    }
  }
};
