export const patchEvent = ( el, key, value ) => {
  // 对函数的缓存
  const invokers = el._vei || ( el._vei = {} );
  const exists = invokers[key];
  if ( value && exists ) {
    // 以前绑定了事件, 更新
    exists.value = value;
  } else {
    const eventName = key.slice( 2 ).toLowerCase();
    if ( value ) {
      // 要绑定事件 以前没有绑定过
      let invoker = ( invokers[key] = createInvoker( value ) );
      el.addEventListener( eventName, invoker );
    } else {
      // 以前绑定了,没有value
      el.removeEventListener( eventName, exists );
      invokers[key] = undefined;
    }
  }
};
function createInvoker( value ) {
  const invoker = ( e ) => {
    invoker.value( e );
  };
  invoker.value = value; // 为了能随时更改value属性
  return invoker;
}
