import { isArray, isObject, isString, ShapeFlags } from "@vue/shared";

export function isVnode( vnode ) {
  return vnode.__v_isVnode
}

// h('div',{style:{color:red}},'children'); //  h方法和createApp类似
export function createVNode( type, props, children = null ) {
  // 标记节点类型
  let shapeFlag = isString( type )
    ? ShapeFlags.ELEMENT
    : isObject( type )
      ? ShapeFlags.STATEFUL_COMPONENT
      : 0;

  // 虚拟节点有跨平台的能力
  const vnode = {
    __v_isVnode: true, // 他是一个vnode节点
    type, // html标签、或者组件名称
    props,
    children,
    component: null, // 组件对应的实例
    el: null, // 真实的dom,更新时复用
    key: props?.key, // diff算法会用到key
    shapeFlag, // 位运算，可以判断同时判断自己和子元素的类型
  };

  // 标记子元素类型
  normalizeChildren( vnode, children );
  return vnode;
};


function normalizeChildren( vnode, children ) {
  let type = 0;
  if ( children == null ) {
  } else if ( isArray( children ) ) {
    type = ShapeFlags.ARRAY_CHILDREN;
  }else if(isObject(children)){
    // 子节点是啥槽
    type = ShapeFlags.SLOTS_CHILDREN;
  }else {
    type = ShapeFlags.TEXT_CHILDREN;
  }
  vnode.shapeFlag = vnode.shapeFlag | type;
}



export const Text = Symbol( "Text" );

// 如果是虚拟节点，直接返回
// 如果是文本，构建一个文件的VNode
export function normalizeVNode( child ) {
  if ( isObject( child ) ) return child;
  return createVNode( Text, null, String( child ) );
}

// 判断节点类型、key是否一致
export const isSameVNodeType = ( n1, n2 ) => {
  return n1.type === n2.type && n1.key === n2.key;
};