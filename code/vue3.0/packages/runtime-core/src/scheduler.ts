let queue = [];
export function queueJob( job ) {
  // 去重
  if ( !queue.includes( job ) ) {
    queue.push( job );
    queueFlush();
  }
}
let isFlushPending = false;
function queueFlush() {
  // 保证只执行一次
  if ( !isFlushPending ) {
    isFlushPending = true;
    // 同步任务结束后，执行异步
    Promise.resolve().then( flushJobs );
  }
}

function flushJobs() {
  // 清空时  我们需要根据调用的顺序依次刷新  , 保证先刷新父在刷新子
  queue.sort( ( a, b ) => a.id - b.id );
  for ( let index = 0; index < queue.length; index++ ) {
    const job = queue[index];
    job();
  }
  isFlushPending = false;
  queue.length = 0;
}
