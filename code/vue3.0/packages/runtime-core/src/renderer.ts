import { effect } from "@vue/reactivity";
import { ShapeFlags } from "@vue/shared";
import { createAppAPI } from "./apiCreateApp";
import { createComponentInstance, setupComponent } from "./component";
import { isSameVNodeType, normalizeVNode, Text } from "./vnode";
import { queueJob } from "./scheduler";
import { invokeArrayFns } from "./apiLifecycle";

function createRenderer( rendererOptions ) {
  // dom 操作
  const {
    insert: hostInsert,
    remove: hostRemove,
    patchProp: hostPatchProp,
    createElement: hostCreateElement,
    createText: hostCreateText,
    createComment: hostCreateComment,
    setText: hostSetText,
    setElementText: hostSetElementText,
    nextSibling: hostNextSibling,
  } = rendererOptions;

  // -------------------文本----------------------
  const processText = ( n1, n2, container ) => {
    if ( n1 == null ) {
      hostInsert( ( n2.el = hostCreateText( n2.children ) ), container );
    }
  };

  // -------------------节点----------------------
  function processElement( n1, n2, container, anchor ) {
    if ( n1 == null ) {
      // 初次渲染
      mountElement( n2, container, anchor );
    } else {
      // 元素类型一样 新老虚拟节点对比
      patchElement( n1, n2 );
    }
  }
  // 新老虚拟节点对比
  function patchElement( n1, n2 ) {
    // 元素类型一样直接复用
    let el = ( n2.el = n1.el );
    // 更新属性  更新儿子
    const oldProps = n1.props || {};
    const newProps = n2.props || {};
    // 对比属性
    patchProps( oldProps, newProps, el );
    // 对比子虚拟节点
    patchChildren( n1, n2, el, parent );
  }

  // 对比属性
  const patchProps = ( oldProps, newProps, el ) => {
    if ( oldProps !== newProps ) {
      // 如果不相等新的属性 需要覆盖掉老的
      for ( const key in newProps ) {
        const prev = oldProps[key];
        const next = newProps[key];
        if ( prev !== next ) {
          hostPatchProp( el, key, prev, next );
        }
      }

      // 老的有的属性 新的没有 将老的删除掉
      for ( const key in oldProps ) {
        if ( !newProps[key] ) {
          hostPatchProp( el, key, oldProps[key], null );
        }
      }
    }
  };

  // 对比子虚拟节点
  let patchChildren = ( n1, n2, el, parent ) => {
    const c1 = n1.children; // 老子虚拟节点
    const c2 = n2.children; // 新子虚拟节点
    const prevShapeFlag = n1.shapeFlag; // 老元素的类型
    const shapeFlag = n2.shapeFlag; // 新元素的类型

    if ( shapeFlag & ShapeFlags.TEXT_CHILDREN ) {
      // 如果新的子节点是文本，老的是列表，需要删除老的真实dom
      if ( prevShapeFlag & ShapeFlags.ARRAY_CHILDREN ) {
        unmountChildren( c1, parent );
      }
      // 如果新、老子元素都是文本，且不一样，直接构建新的文本
      if ( c2 !== c1 ) {
        hostSetElementText( el, c2 );
      }
    } else {
      if ( prevShapeFlag & ShapeFlags.ARRAY_CHILDREN ) {
        // 如果新老子虚拟节点，都是列表，diff对比
        if ( shapeFlag & ShapeFlags.ARRAY_CHILDREN ) {
          patchKeyedChildren( c1, c2, el, parent );
        } else {
          // 如果老的虚拟节点是列表，新的不是删除老的真实dom
          unmountChildren( c1, parent );
        }
      } else {
        // 新的不是文本，老的是文本删除
        if ( prevShapeFlag & ShapeFlags.TEXT_CHILDREN ) {
          hostSetElementText( el, "" );
        }
        // 新的是数组
        if ( shapeFlag & ShapeFlags.ARRAY_CHILDREN ) {
          mountChildren( c2, el );
        }
      }
    }
  };


  // diff对比
  function patchKeyedChildren( c1, c2, el, parent ) {
    let i = 0; // 都是默认从头开始比对
    let e1 = c1.length - 1;
    let e2 = c2.length - 1;
    // 头头对比
    while ( i <= e1 && i <= e2 ) {
      const n1 = c1[i];
      const n2 = c2[i];
      if ( isSameVNodeType( n1, n2 ) ) {
        patch( n1, n2, el,parent )
        i++
      } else {
        break
      }
    }

    //尾尾对比
    while ( i <= e1 && i <= e2 ) {
      const n1 = c1[e1];
      const n2 = c2[e2];
      if ( isSameVNodeType( n1, n2 ) ) {
        patch( n1, n2, el, )
        e1--
        e2--
      } else {
        break
      }
    }
    if ( i > e1 ) {
      // 老的虚拟节点已经对比完成，可能有新增的
      let nextPos = e2 + 1;
      // 如果当前在插入的节点，后面有节点，插入在节点之前，如果没有直接插入在最后
      let anchor = c2[nextPos] ? c2[nextPos].el : null
      while ( i <= e2 ) {
        patch( null, c2[i], el, anchor );
        i++;
      }
    } else if ( i > e2 ) {
      // 新的拟节点已经对比完成，可能需要删除老的多余的
      while ( i <= e1 ) {
        unmount( c1[i], parent );
        i++;
      }
    } else {
      // 新老虚拟节头头、尾尾对比之后都没对比完成，可能中间的节点可以复用
      // (A B)[E C] (D)
      // (A B)[G E H] (D)
      let s1 = i;
      let s2 = i;
      // 乱序比较，需要尽可能复用，用新的虚拟列表做成一个映射表
      const keyToNewIndexMap = new Map();
      for ( let index = s2; index <= e2; index++ ) {
        const childVNode = c2[index]; // child
        // {'G':2,'E':3,'H':4}
        keyToNewIndexMap.set( childVNode.key, index ); // 新的虚拟dom key，对应的位置
      }
      // 计算出还有多少新的虚拟列表未处理
      const toBePatched = e2 - s2 + 1;
      // 根据剩余长度的创建一个数组
      const newIndexToOldIndexMap = new Array( toBePatched ).fill( 0 );
      // 遍历老的虚拟列表，尽可能的复用
      for ( let index = s1; index <= e1; index++ ) {
        const childVNode = c1[index]; // child
        const newIndex = keyToNewIndexMap.get( childVNode.key );
        if ( newIndex ) {
          // 标记已经复用过，未标记过的都是0
          // 在后续处理剩余新的元素，是从剩余的个数遍历的需要正确标记位置
          newIndexToOldIndexMap[newIndex - s2] = index + 1;
          // 复用老的dom
          patch( childVNode, c2[newIndex], el );
        } else {
          // 不能复用直接删除
          unmount( childVNode, parent );
        }
      }
      // 插入新的元素，移动复用的元素到正确的位置
      for ( let i = toBePatched - 1; i >= 0; i-- ) {
        const nextIndex = s2 + i; // (A B)[G E H]  找到H的索引
        const nextChild = c2[nextIndex]; // 找到H
        let anchor = nextIndex + 1 < c2.length ? c2[nextIndex + 1].el : null; // 找到当前元素的下一个元素
        // 未标记过，直接插入新的节点
        if ( newIndexToOldIndexMap[i] === 0 ) {
          patch( null, nextChild, el, anchor );
        } else {
          // 已经标记过，移动元素到正确的位置
          hostInsert( nextChild.el, el, anchor );
        }
      }

    }
  }

  // 创建真的的dom
  const mountElement = ( vnode, container, anchor = null ) => {
    const { props, shapeFlag, type, children } = vnode;
    // 创建真的dom,并且给把真实的dom挂载到el属性上，更新时复用
    let el = ( vnode.el = hostCreateElement( type ) );
    // 处理props
    if ( props ) {
      for ( const key in props ) {
        hostPatchProp( el, key, null, props[key] );
      }
    }
    // 处理子元素
    if ( shapeFlag & ShapeFlags.TEXT_CHILDREN ) {
      // 文本比较简单 直接扔进去即可
      hostSetElementText( el, children );
    } else if ( shapeFlag & ShapeFlags.ARRAY_CHILDREN ) {
      // 如果子元素是一个列表, 递归渲染
      mountChildren( children, el );
    }
    // 将真实的dom渲染到页面
    hostInsert( el, container, anchor );
  };

  // 处理子节点
  const mountChildren = ( children, container ) => {
    for ( let i = 0; i < children.length; i++ ) {
      // 如果是虚拟节点，直接返回虚拟节点
      // h('div',h('div'))

      // 如果是文本，构建一个文件的VNode
      // h('div','hu.lei')
      let child = normalizeVNode( children[i] );
      patch( null, child, container );
    }
  };

  // -------------------组件----------------------
  function processComponent( n1, n2, container, parent ) {
    if ( n1 == null ) {
      if ( n2.shapeFlag & ShapeFlags.COMPONENT_KEPT_ALIVE ) {
        
        // 在第一次卸载的时候，我们已经将dom元素移动到内存中，这次渲染我们再将他拿回来
        parent.ctx.active( n2, container );
      } else {
        // 初次渲染
        mountComponent( n2, container, parent );
      }

    } else {
      console.log( "组件更新" );
    }
  }

  // 组件初次渲染
  function mountComponent( initialVNode, container, parent ) {
    // 1、根据VNode创建组件实例
    let instance = ( initialVNode.component =
      createComponentInstance( initialVNode, parent ) );


    // keepAlive
    instance.ctx.renderer = {
      createElement: hostCreateElement, // 创建元素
      move( vnode, container ) { // 移动dom
        hostInsert( vnode.component.subTree.el, container )
      },
      unmount // 卸载dom
    }
    // 2、需要将VNode的数据挂载到组件实例上
    setupComponent( instance )
    // 3、创建一个effect 把渲染权交给effect
    setupRenderEfect( instance, container );
  }

  // 每一个组件对应一个effect
  function setupRenderEfect( instance, container ) {
    // 组件更新时调用
    instance.update = effect( function componentEffect() {
      if ( !instance.isMounted ) {
        let { bm, m } = instance;
        if ( bm ) {
          invokeArrayFns( bm );
        }
        // 初次渲染
        instance.isMounted = true
        let proxyToUse = instance.proxy;
        // 调用render转换成虚拟dom，如果响应式数据有取值操作，会收集当前的effect，让响应式数据发生改变时，通知effect更新视图
        // 真实dom，对应的虚拟dom，更新时diff对比
        let subTree = ( instance.subTree = instance.render.call(
          proxyToUse,
          proxyToUse
        ) );
        patch( null, subTree, container, null, instance );
        if ( m ) {
          invokeArrayFns( m );
        }
      } else {
        // 响应式数据发生变化时，通知effect更新视图

        let { bu, u } = instance;

        if ( bu ) {
          invokeArrayFns( bu );
        }

        // 老的虚拟节点
        const prevTree = instance.subTree;
        // 新的虚拟节点
        let proxyToUse = instance.proxy;
        let nextTree = ( instance.subTree = instance.render.call(
          proxyToUse,
          proxyToUse
        ) );
        // 再次赋值，用于下次对比
        instance.subTree = nextTree;
        patch( prevTree, nextTree, container,null,instance );
        if ( u ) {
          invokeArrayFns( u );
        }
      }
    }, {
      // 如果同时有多个响应式数据发生变化，会触发effect多次执行，我们只需要执行一次即可
      // 利用事件循环机制
      schedular: queueJob,
    } )
  }

  // 删除老的节点
  function unmount( n1, parent ) {
    let { shapeFlag } = n1;
    // 被缓存的组件不会真实卸载，内存中 KEEP_ALIVE
    if ( shapeFlag & ShapeFlags.COMPONENT_SHOULD_KEEP_ALIVE ) {
      parent.ctx.deactivate( n1 );
    }else{
    // 如果是组件 调用的组件的生命周期等(组件情况)
    hostRemove( n1.el );
    }

  };

  // 遍历删除子元素
  function unmountChildren( children, parent ) {
    for ( let index = 0; index < children.length; index++ ) {
      unmount( children[index], parent );
    }
  };

  /**
   * 
   * @param n1 老的虚拟节点
   * @param n2 新的虚拟节点
   * @param container 父容器
   * @param anchor 参照物
   * @param parent 父节点实例
   */
  function patch( n1, n2, container, anchor = null, parent = null ) {
    let { shapeFlag, type } = n2

    // 如果新老节点key不一致，直接走收次渲染逻辑
    if ( n1 && !isSameVNodeType( n1, n2 ) ) {
      anchor = hostNextSibling( n1.el );
      unmount( n1, parent ); // 清空真实的老节点
      n1 = null; // 清空老的虚拟节点
    }
    if ( Text === type ) {
      // 文本
      processText( n1, n2, container );
    } else if ( shapeFlag & ShapeFlags.ELEMENT ) {
      // 元素
      processElement( n1, n2, container, anchor );
    } else if ( shapeFlag & ShapeFlags.STATEFUL_COMPONENT ) {
      // 组件
      processComponent( n1, n2, container, parent );
    }

  }

  // 首次渲染
  const render = ( vnode, container ) => {
    patch( null, vnode, container )
  };

  return {
    createApp: createAppAPI( render ),
  }
}

export {
  createRenderer
}

