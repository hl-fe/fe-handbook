import { currentInstance as instance } from "./component";
// provide or inject必须在组件中使用

/**
 * 内容提供者
 * @param key 消费的key
 * @param value 消费的值
 * @returns 
 */
export function provide( key, value ) {
  if ( !instance ) return;
  let parentProvides = instance.parent && instance.parent.provides;
  let currentProvides = instance.provides;

  if ( currentProvides === parentProvides ) {
    currentProvides = instance.provides = Object.create( parentProvides );
  }
  currentProvides[key] = value
}

/**
 * 消费者
 * @param key 内容提供者key
 * @param defaultValue 默认值
 * @returns 
 */
export function inject( key, defaultValue ) {
  if ( !instance ) return;
  // 取组件提供的provide，如果没有返回默认值
  const provides = instance.parent?.provides;
  if ( provides && ( key in provides ) ) {
    return provides[key];
  } else {
    return defaultValue
  }
}
