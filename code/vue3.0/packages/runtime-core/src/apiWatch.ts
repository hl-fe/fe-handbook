import { effect } from "@vue/reactivity";
import { hasChanged, isFunction } from "@vue/shared";
import { queueJob } from "./scheduler";

/**
 * 
 * @param source 需要监听的数据(可能是函数，有可以是值)
 * @param cb 数据变化后的回调
 * @param options 配置项
 * @returns 
 */


// watch( () => count, ( newValue, oldValue ) => {
// }, {
//   deep: true,
//   immediate: true
// } );
function watch( source, cb, options = {} ) {
  return doWatch( source, cb, options )
}

// 默认会执行一次
function watchEffect( source ) {
  return doWatch( source, null, {} )
}

function doWatch( source, cb, options ) {
  // 如果不是函数包装成函数，方便后续处理
  let sourceHandler = source;
  if ( !isFunction( source ) ) {
    sourceHandler = () => source
  }
  let oldValue = '' // 老值
  // 自定义effect执行器
  let schedular = () => {
    if ( cb ) {
      // 或者最新的值
      let newValue = runner();
      // 如果新老值不一样，执行回调
      if ( hasChanged( oldValue, newValue ) ) {
        cb( newValue, oldValue );
        oldValue = newValue;
      }
    } else {
      // watchEffect没有回调，直接执行source
      sourceHandler();
    }
  };
  let runner = effect( sourceHandler, {
    lazy: true,
    schedular: () => {
      // 如果数据同一时间多次变更，会触发effect多次执行，我们只需要执行一次即可
      queueJob( schedular );
    },
  } )

  // 是否默认执行一次watch回调
  if ( options?.immediate ) {
    schedular()
  }
  // 或者监听的值，同时触发收集当前的effect，当监听的值发生变化时通知effect执行回调
  oldValue = runner()

}

export {
  watch,
  watchEffect
}