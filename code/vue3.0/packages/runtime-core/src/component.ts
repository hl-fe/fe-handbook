import { isFunction, isObject, ShapeFlags } from "@vue/shared";
import { PublicInstanceProxyHandlers } from "./PublicInstanceProxyHandlers";
import { reactive } from "@vue/reactivity";


// 组件实例
export let currentInstance;
export const getCurrentInstance = () => {
  return currentInstance;
};
export const setCurrentInstance = ( instance ) => {
  currentInstance = instance;
};

// 根据Vnode创建组件实例
let uuid = 0;
function createComponentInstance( vnode,parent ) {
  // createApp({
  //   setup(props, context) {
  //     return (ctx) => {
  //       return h("li", { key: "A" }, "A");
  //     };
  //     // OR
  //     let state = reactive({ name: "hl" });
  //     return {
  //       state
  //     }
  //   },
  //   render(ctx) {
  //     return h("li", { key: "A" }, "A");
  //   },
  // });
  const type = vnode.type;// 组件
  // 组件实例
  const instance = {
    uuid: uuid++,
    __v_isVNode: true,
    vnode, // 组件对应的虚拟节点
    subTree: null, // 真实dom，对应的虚拟dom
    type, // 组件对象
    propsOptions: vnode.type.props || {}, // 组件中接受的属性
    ctx: {} as any, // 组件的上下文
    props: {}, // 组件的属性
    attrs: {}, // 元素本身的属性
    slots: {}, // 组件的插槽
    setupState: {}, // 组件setup的返回值
    isMounted: false, // 组件是否被挂载
    exposed: {}, // 暴露子组件的函数
    parent, // 标记当前组件的父亲是谁
    provides:parent ? parent.provides : Object.create(null) // 父 -》 儿子 -》 孙子用的是同一个对象
  };
  instance.ctx = { _: instance };
  return instance;
}


const initProps = (instance, userProps) => {
  const attrs = {};
  const props = {};
  const options = instance.propsOptions || {}; // 组件上接受的props
  if (userProps) {
    for (let key in userProps) {
      // 属性中应该包含属性的校验
      const value = userProps[key];
      if (key in options) {
        props[key] = value;
      } else {
        attrs[key] = value;
      }
    }
  }
  instance.attrs = attrs;
  instance.props = reactive(props);
};

// 如果有插槽，将插槽存储在组件实例slots上
const initSlots = (instance,children)=>{
  if(instance.vnode.shapeFlag & ShapeFlags.SLOTS_CHILDREN){ // 包含插槽 则说明children 是插槽
    instance.slots = children; // 将用户的children 映射到实例上
}
}

// 解析vnode将数据挂载到组件实例上
function setupComponent( instance ) {
  let { props, children } = instance.vnode;
  // 根据props 解析出 props 和 attrs
  // 如果子组件有接收props，将会实例在props上，否则全部在实例attrs上
  initProps(instance,props)
  initSlots(instance,children); // 插槽的解析

  let isStateful = instance.vnode.shapeFlag & ShapeFlags.STATEFUL_COMPONENT;
  if ( isStateful ) {
    setupStatefulComponent( instance );
  }
}


// 处理setup函数
export function setupStatefulComponent( instance ) {
  // 1.代理 传递给render函数的参数
  instance.proxy = new Proxy( instance.ctx, PublicInstanceProxyHandlers );
  let Component = instance.type;
  let { setup } = Component;
  // 优先处理setup
  if ( setup ) {
    // 保存当前组件的实例，实例上有挂载生命周期回调队列
    currentInstance = instance;
    // 1、构建setup 下上文content
    let setupContext = createSetupContext( instance );
    // 2、执行setup，的要setup返回结果
    const setupResult = setup( instance.props, setupContext );
    // 重置，防止父子组件冲突
    currentInstance = null;
    // 3、处理setup返回值
    handleSetupResult( instance, setupResult );
  } else {
    // 如果没有setup函数，
    finishComponentSetup( instance )
  }

}

// 构建setup 下上文content
// setup(props, content) {
//    const { attrs, slots, emit, expose } = content
//  }
function createSetupContext( instance ) {
  return {
    attrs: instance.attrs,
    slots: instance.slots,
    emit: (eventName, ...args) => { 
      let bindName = `on${eventName[0].toUpperCase()}${eventName.slice(1)}`;
        const handler = instance.attrs[bindName];

        if (handler) {
          let handlers = Array.isArray(handler) ? handler : [handler];
          handlers.forEach((handler) => handler(...args));
        }
    },
    expose(exposed) {
      // 主要用于ref ，通过ref获取组件的时候 在vue里只能获取到组件实例，但是在vue3中如果提供了
      // exposed 则获取的就是exposed属性
      instance.exposed = exposed;
    },
  }
}

// 处理setup返回值
function handleSetupResult( instance, setupResult ) {
  // 如果setup函数返回结果是一个函数，作为render函数
  if ( isFunction( setupResult ) ) {
    instance.render = setupResult;
  } else if ( isObject( setupResult ) ) {
    // 如果是对象，把结果赋值给setupState
    instance.setupState = setupResult;
  }
  //  处理后可能依旧没有render函数
  // 1）用户没写setup
  // 2) 用户写了setup但是什么都没返回
  finishComponentSetup( instance );
}

// render函数
function finishComponentSetup( instance ) {
  const Component = instance.type;
  if ( !instance.render ) {
    if ( Component.template && !Component.render ) {
      // 模板编译，需要将template 变成render函数  compileToFunctions()
    }
    instance.render = Component.render;
  }
  // applyOptions 2.0API兼容处理
  // applyOptions(instance,Component);
}



export {
  createComponentInstance,
  setupComponent
}

