import { currentInstance, setCurrentInstance } from "./component";

const enum LifeCycles {
  BEFORE_MOUNT = "bm", // 组件渲染前
  MOUNTED = "m", // 组件渲染后
  BEFORE_UPDATE = "bu", // 组件更新前
  UPDATED = "u", // 组件更新后
}
// 生命周期只能在setup中使用
// 因为只有在调用setup时才会生成组件的实例，currentInstance，并且生命周期挂载组件实例上

// 订阅
function createHook( lifeCycle ) {
  /**
   * hook v钩子类型
   * currentInstance 当前组件的实例
   */
  return ( hook, target = currentInstance ) => {
    injectHook( lifeCycle, hook, target );
  };
}

function injectHook( lifecycle, hook, target ) {
  if ( !target ) return;
  // 一个组件同一类型生命周期可以多个
  const hooks = target[lifecycle] || ( target[lifecycle] = [] );
  const wrap = () => {
    setCurrentInstance( target );
    hook();
    setCurrentInstance( null );
  };
  hooks.push( wrap );
}

// 发布
export function invokeArrayFns( fns ) {
  fns.forEach( ( fn ) => fn() );
}

export const onBeforeMount = createHook( LifeCycles.BEFORE_MOUNT );
export const onMounted = createHook( LifeCycles.MOUNTED );
export const onBeforeUpdate = createHook( LifeCycles.BEFORE_UPDATE );
export const onUpdated = createHook( LifeCycles.UPDATED );
