export { createRenderer } from "./renderer";
export { h } from "./h";
export { watch, watchEffect } from "./apiWatch";
export * from "@vue/reactivity";
export { getCurrentInstance } from "./component";
export * from "./apiLifecycle";
export * from "./apiInject";
export * from "./keepAlive";