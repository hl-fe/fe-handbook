import { createVNode } from "./vnode";

export function createAppAPI( render ) {

  return ( rootComponent, rootProps = null ) => {
    const app = {
      _props: rootProps,
      _component: rootComponent,
      _container: null,
      mount( rootContainer ) {
        // 1.通过rootComponent 创建vnode
        // 2.调用render方法将vnode渲染到rootContainer中
        const vnode = createVNode( rootComponent, rootProps, null );
        render( vnode, rootContainer );
        app._container = rootContainer;
      },
    };
    return app
  }
}


