import { isArray, isObject } from "@vue/shared";
import { createVNode, isVnode } from "./vnode";
// h函数转虚拟
// 1.  只有两个参数  类型 + 孩子  / 类型 + 属性
// 2.  三个参数 最后一个不是数组
// 3.  超过三个 多个参数
// h('div','hello')
// h('div','h('hello')')
// h('div','[hello1,hell2,hell3]')

// h('div',{},'hello',hello)
// h('div',{},['hello',hello])
export function h( type, propsOrChildren?, children? ) {
  const l = arguments.length;
  if ( l == 2 ) {
    // 类型 + 属性 、  类型 + 孩子
    // 如果是对象、不是数组
    if ( isObject( propsOrChildren ) && !isArray( propsOrChildren ) ) {
      // 如果是虚拟节点、就是子元素
      if ( isVnode( propsOrChildren ) ) {
        return createVNode( type, null, [propsOrChildren] );
      }
      // 不是虚拟节点就是属性
      return createVNode( type, propsOrChildren );
    } else {
      // 如果第二个参数 不是对象 那一定是孩子
      return createVNode( type, null, propsOrChildren );
    }
  } else {
    // 从三个开始后面都是子元素
    if ( l > 3 ) {
      children = Array.prototype.slice.call( arguments, 2 );
    } else if ( l === 3 && isVnode( children ) ) {
      children = [children];
    }
    return createVNode( type, propsOrChildren, children );
  }
}
