import { Component } from 'react'
import { connect } from './react-redux'
import counter1 from './store/actionCreators/counter1'
import counter2 from './store/actionCreators/counter2'
class App extends Component {
  render () {
    return (
      <>
        <div>
          <button onClick={this.props.add1}>add1</button>
          <button onClick={this.props.minus1}>minus1</button>
          <button onClick={this.props.add1Async}>add1Async</button>
          <button onClick={this.props.add1Promise}>add1Promise</button>
          {this.props.counter1.number}
        </div>
        <hr />
        <div>
          <button onClick={this.props.add2}>add1</button>
          <button onClick={this.props.minus2}>minus1</button>
          {this.props.counter2.number}
        </div>
      </> )

  }
}

const mapStateToProps = ( state ) => state


// const mapDispatchToProps = (dispatch) => {
//   return {
//     add1: () => dispatch( counter1.add1() ),
//     add2: () => dispatch( counter2.add2() ),
//     minus1: () => dispatch( counter1.minus1() ),
//     minus2: () => dispatch( counter2.minus2() )
//   }
// }

// mapDispatchToProps = {add1: ƒ, minus1: ƒ, add2: ƒ, minus2: ƒ}
// 需要把action动作，转换成dispatch能触发的动作
const mapDispatchToProps = {
  ...counter1,
  ...counter2
}
export default connect( mapStateToProps, mapDispatchToProps )( App )
