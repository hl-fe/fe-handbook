export default function thunk ( { getState, dispatch } ) {
  return (nextDispatch) => {
    return (action)=>{
      if(typeof action ==='function'){
        action(nextDispatch)
      }else{
        nextDispatch(action)
      }
    }
  }
}