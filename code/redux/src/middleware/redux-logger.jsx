export default function logger ( { getState, dispatch } ) {
  return (nextDispatch) => {
    return (action)=>{
      console.log('更新前',getState())
      nextDispatch(action)
      console.log('更新后',getState())
    }
  }
}