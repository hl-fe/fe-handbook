

export function persistenceState (storageType='localStorage') {
  return ( { getState } ) => {
    return ( nextDispatch ) => {
      return ( action ) => {
        nextDispatch( action )
        try {
          window[storageType].setItem( 'REDUX_PERSISTENCE', JSON.stringify( getState() ) )
        } catch ( error ) {
          console.log( error )
        }
      }
    }
  }
}


export function restoreState(){
  return JSON.parse(localStorage.getItem('REDUX_PERSISTENCE') || '{}')
}
