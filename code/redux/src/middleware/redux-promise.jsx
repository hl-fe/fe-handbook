export default function thunk ( { getState, dispatch } ) {
  return ( nextDispatch ) => {
    return ( action ) => {
      if ( action.then && typeof action.then==='function' ) {
        action.then(nextDispatch)
      } else {
        nextDispatch( action )
      }

    }
  }
}