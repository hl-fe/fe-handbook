import { ADD1, MINUS1 } from '../action-types';
function add1 () {
  return { type: ADD1 };
}

function add1Async () {
  return (dispatch)=>{
    setTimeout(()=>{
      dispatch({ type: ADD1 })
    },1000)
  }
}

function add1Promise() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve({ type: ADD1 });
    }, 1000);
  });
}


function minus1 () {
  return { type: MINUS1 };
}
const actionCreators = { add1, minus1,add1Async,add1Promise };
export default actionCreators;