import { ADD1, MINUS1 } from '../action-types';
let initState = { number: 0 };
const reducer = (state = initState, action) => {
  switch (action.type) {
    case ADD1:
      return { number: state.number + 1 };
    case MINUS1:
      return { number: state.number - 1 };
    default:
      return state;
  }
}
export default reducer;