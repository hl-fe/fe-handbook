
import { createStore, applyMiddleware } from '../redux';
import { reduxLogger, reduxThunk, reduxPromise, persistenceState,restoreState } from '../middleware';
import combinedReducer from './reducers';
const store = applyMiddleware( reduxPromise, reduxThunk, reduxLogger,persistenceState() )( createStore )( combinedReducer,restoreState() )
export default store;