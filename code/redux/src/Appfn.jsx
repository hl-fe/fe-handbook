import { useSelector,useBoundDispatch } from './react-redux';
import counter1 from './store/actionCreators/counter1'
import counter2 from './store/actionCreators/counter2'

export default function AppFn () {
  const state = useSelector(state => state);
  const { add1, minus1,add1Async,add1Promise,add2,minus2 } = useBoundDispatch({ ...counter1,...counter2});
  return (
    <>
      <div>
        <button onClick={add1}>add1</button>
        <button onClick={minus1}>minus1</button>
        <button onClick={add1Async}>add1Async</button>
        <button onClick={add1Promise}>add1Promise</button>
        {state.counter1.number}
      </div>
      <hr />
      <div>
        <button onClick={add2}>add1</button>
        <button onClick={minus2}>minus1</button>
        {state.counter2.number}
      </div>
    </> )


}