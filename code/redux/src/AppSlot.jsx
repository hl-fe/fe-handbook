const useSlot = (props)=>{
  let childrens = []
  if(props.children){
    if(Array.isArray(props.children)){
      childrens = props.children
    }else{
      childrens = [props.children]
    }
  }
  let slotMap = {}
  childrens.forEach(children => {
    slotMap[children.props.slot || 'default'] = children
  });
  return slotMap
}



function Layout (props) {
  const slot = useSlot(props)
  return (
    <>
    {slot.head}
    {slot.middle}
    {slot.tail}
    {slot.default}
    </> )
}


export default function AppSlot () {
  return (
    <>
    <Layout>
      <h1>默认</h1>
      <h1 slot='head'>头部</h1>
      <h1 slot='middle'>中间</h1>
      <h1 slot='tail'>尾部</h1>
      </Layout>
    </> )
}