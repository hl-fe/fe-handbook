/**
 * 合并多个reducers
 * @param {*} reducers 
 */
export default function combineReducers ( reducers ) {
  /**
  * createStore执行dispatch触发
  * @param {*} state 老的state状态
  * @param {*} action action动作
  */
  return function combination ( state = {}, action ) {
    // 总的state
    let nextState = {}
    // TODO:缺陷每次触发dispatch，所有reducer都会执行一遍
    for ( const key in reducers ) {
      const preState = state[ key ]// 老的state状态
      nextState[ key ] = reducers[ key ]( preState, action )
    }
    return nextState
  }

}