export default function compose(...funcs) {
  if (funcs.length === 0) {
    return args => args
  } else if (funcs.length === 1) {
    return funcs[0]
  }
  return funcs.reduce((a, b) => (dispatch) => a(b(dispatch)))
}


// function compose(...funcs) {
//   return function (dispatch) {
//     for (let i = funcs.length - 1; i >= 0; i--) {
//       dispatch = funcs[i](dispatch);
//     }
//     return dispatch;
//   }
// }
