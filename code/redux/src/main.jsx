import React from 'react'
import ReactDOM from 'react-dom/client'
import {Provider} from './react-redux'
import App from './App'
import AppFn from './AppFn'
import AppSlot from './AppSlot'
import store from './store'

ReactDOM.createRoot(document.getElementById('root')).render(
  <Provider store={store}>
    <App />
    <hr />
    <AppFn />
    <hr />
    <AppSlot/>
  </Provider>,
)
