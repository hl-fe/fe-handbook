import { useContext } from 'react';
import ReactReduxContext from '../ReactReduxContext';
export default function useBoundDispatch ( actionCreators ) {
  const { store } = useContext( ReactReduxContext );
  const boundActionCreators = {}
  // 将action动作，转换成dispatch能触发的动作
  for ( const key in actionCreators ) {
    boundActionCreators[ key ] = () => store.dispatch( actionCreators[ key ]() )
  }

  return boundActionCreators;
}