import React, {  useLayoutEffect, useState } from 'react';
import ReactReduxContext from '../ReactReduxContext';

function useSyncExternalStore(subscribe, getSnapShot) {
  let [state, setState] = useState(getSnapShot());
  useLayoutEffect(() => {
      subscribe(() => {
          setState(getSnapShot())
      });
  }, []);
  return state;
}

export default function useSelector(selector) {
    const { store } = React.useContext(ReactReduxContext);
    return useSyncExternalStore(
        store.subscribe,
        () => selector(store.getState())
    );
}


