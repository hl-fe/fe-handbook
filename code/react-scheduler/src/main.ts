import {
  unstable_ImmediatePriority as ImmediatePriority,
  unstable_UserBlockingPriority as UserBlockingPriority,
  unstable_NormalPriority as NormalPriority,
  unstable_LowPriority as LowPriority,
  unstable_IdlePriority as IdlePriority,
  unstable_scheduleCallback as scheduleCallback,
  unstable_shouldYield as shouldYield,
  CallbackNode,
  unstable_getFirstCallbackNode as getFirstCallbackNode,
  unstable_cancelCallback as cancelCallback,
} from "scheduler";
import "./style.css";

// 从DOM中获取根元素
const root = document.querySelector("#root");

// 任务列表，用于跟踪任务
const workList: Work[] = [];

// 用于跟踪上一个任务的优先级和当前回调
let prevPriority: Priority = IdlePriority;
let curCallback: CallbackNode | null = null;

// 定义优先级类型
type Priority =
  | typeof IdlePriority
  | typeof LowPriority
  | typeof NormalPriority
  | typeof UserBlockingPriority
  | typeof ImmediatePriority;

// 定义任务的接口，包含计数和优先级
interface Work {
  count: number;
  priority: Priority;
}

// 为每个优先级创建按钮并添加到根元素
[LowPriority, NormalPriority, UserBlockingPriority, ImmediatePriority].forEach(
  (priority) => {
    const btn = document.createElement("button");
    root?.appendChild(btn);
    btn.innerText = [
      "",
      "ImmediatePriority",
      "UserBlockingPriority",
      "NormalPriority",
      "LowPriority",
    ][priority];
    btn.onclick = () => {
      console.log("-hcc---priority--priority", priority);
      // 添加一个新的任务到任务列表，初始计数为100，优先级为所选优先级
      workList.unshift({
        count: 100,
        priority: priority as Priority,
      });
      // 调度任务
      schedule();
    };
  }
);

function schedule() {
  const cbNode = getFirstCallbackNode();
  // 获取任务列表中优先级最高的任务
  const curWork = workList.sort((w1, w2) => w1.priority - w2.priority)[0];

  // 如果没有任务，取消当前回调并返回
  if (!curWork) {
    curCallback = null;
    cbNode && cancelCallback(cbNode);
    return;
  }

  const { priority: currentPriority } = curWork;
  // 如果当前任务的优先级与上一个任务相同，则不做任何操作
  if (currentPriority === prevPriority) {
    return;
  }
  // 如果有更高优先级的任务，取消之前的回调并调度新的任务
  cbNode && cancelCallback(cbNode);
  curCallback = scheduleCallback(currentPriority, perform.bind(null, curWork));
}

function perform(work: Work, didTimeout?: boolean): any {
  /**
   * 中断情况：
   * 1. 存在更高优先级的任务
   * 2. 防止饥饿问题（任务一直得不到执行，优先级不断增加） - didTimeout
   * 3. 时间切片 - 检查当前执行时间是否已经完成 shouldYield
   */
  const needSync = work.priority === ImmediatePriority || didTimeout;
  // 只要是立即优先级或者没有超时，并且任务计数不为零，就执行任务
  // 同步执行无法中断
  while ((needSync || !shouldYield()) && work.count) {
   console.log('x',didTimeout)
    work.count--;
    // 向DOM插入一个span元素
    insertSpan(work.priority + "");
  }
  // 处理中断或完成情况
  prevPriority = work.priority;
  if (!work.count) {
    const workIndex = workList.indexOf(work);
    workList.splice(workIndex, 1);
    prevPriority = IdlePriority;
  }
  const prevCallback = curCallback;
  // 重新调度剩余的任务
  schedule();
  const newCallback = curCallback;
  // 如果新回调与之前相同，继续执行任务
  if (newCallback && prevCallback === newCallback) {
    return perform.bind(null, work);
  }
}

function insertSpan(content: string) {
  // 创建一个span元素，并设置其内容和类名
  const span = document.createElement("span");
  span.innerText = content;
  span.className = `pri-${content}`;
  // 模拟一些繁重的工作
  doSongBuzyWork(10000000);
  // 将span元素添加到根元素中
  root?.appendChild(span);
}

function doSongBuzyWork(len: number) {
  let result = 0;
  // 执行一些计算密集型的任务
  while (len--) {
    result += len;
  }
}
