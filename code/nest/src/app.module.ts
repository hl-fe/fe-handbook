import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { PayModule } from './pay/pay.module';

@Module({
  imports: [UserModule, PayModule],
  controllers: [],
})
export class AppModule {}
