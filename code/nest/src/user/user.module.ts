import { Global, MiddlewareConsumer, Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { LoggerMiddleware } from './../middleWares/logger.middleware';
@Global()
@Module({
  controllers: [UserController],
  providers: [
    UserService,
    {
      provide: 'UserService',
      useClass: UserService,
    },
    {
      provide: 'jobs',
      useValue: ['java', 'fs', 'ios'],
    },
  ],
  exports: [UserService],
})
export class UserModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes(UserController);
  }
}
