import { generate } from "./generate";
import { parserHTML } from "./parser";

export function compileToFunction ( html ) {
  // 1.把模板变成ast语法树  
  const ast = parserHTML( html );
  //  {
  //   type: 1,
  //   tag: 'div',
  //   attrs: [ { name: 'id', value: 'app' }, ... ],
  //   parent: null,
  //   children: [ ...]
  // }
  // 2.优化标记静态节点

  // 3.将ast变成render函数
  const code = generate( ast );
  // with 作用域指向
  const render = new Function( `with(this){return ${ code }}` );
  return render;
}
