let depId = 0
class Dep {
  constructor () {
    this.depId = depId++
    this.watchers = [];
  }

  // 响应式数据在取值时，如果有watcher触发依赖收集
  // 通过watcher来调用的目的是为了去重，让dep和watcher产生关联
  depend () {
    Dep.target.addDep( this );
  }

  // 收集watcher(渲染watcher、watch watcher、计算属性watcher)
  addSub ( watcher ) {
    this.watchers.push( watcher );
  }

  // 响应式数据发生变化，通知watcher更新
  notify () {
    this.watchers.forEach( ( watcher ) => watcher.update() );
  }
}

Dep.target = null;
export let stack = []
export function pushTarget ( watcher ) {
  stack.push( watcher );
  Dep.target = watcher;
}

export function popTarget () {
  stack.pop();
  Dep.target = stack[ stack.length - 1 ];
}

export default Dep