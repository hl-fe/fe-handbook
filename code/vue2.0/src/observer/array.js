
const oldArrayPrototype = Array.prototype;
export const proto = Object.create( oldArrayPrototype );

[ "push", "pop", "unshfit", "shift", "reverse", "sort", "splice" ].forEach( ( method ) => {
  proto[ method ] = function ( ...args ) {
    let ob = this.__ob__;
    let r = oldArrayPrototype[ method ].call( this, ...args );
    let inserted;
    switch ( method ) {
      case "push":
      case "unshift": // 前后新增
        inserted = args;
      case "splice":
        // arr.splice(0,1,新增的内容)
        // 如果是splice,参数大于2个后门的都是新增的值
        inserted = args.slice( 2 );
      default:
        break;
    }
    // 数组发生变化，通知dep更新
    ob.dep.notify()
    // 如果有新增，需要新增的做代理
    if ( inserted ) ob.observeArray( inserted );
    return r;
  }

} )