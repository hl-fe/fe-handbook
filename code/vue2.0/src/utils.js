/**
 * 
 * merge合并策略
 * 除生命周期以外，如果属性相同，后者会覆盖前者
 * 如果是生命周期，会形成一个队列，会按照执行的前后顺序排列
 */
const lifecycles = {};
[ 'beforeCreate', 'created', 'beforeMount', 'mounted' ].forEach( method => {
  // 策略模式
  lifecycles[ method ] = function ( preLifecycle, postLifecycle ) {

    if ( !preLifecycle && postLifecycle ) {
      // 第一次有生命周期合并的时候，上一个肯定是空对象，也需要形成一个队列
      return [ postLifecycle ]
    }
    // 第二次到N次，如果有生命周期按照前后顺序进行排序，如果没有直接把上一次的返回
    if ( postLifecycle ) {
      return preLifecycle.concat( postLifecycle )
    }
    return preLifecycle
  }
} )

export function mergeOptions ( preOptions, postOptions ) {
  const opts = {}

  // 循环处理上一个
  for ( const key in preOptions ) {
    mergeField( key )
  }

  // 循环处理上一个
  for ( const key in postOptions ) {
    // 相同的属性上一个循环已经处理
    if ( !preOptions.hasOwnProperty( key ) ) {
      mergeField( key )
    }
  }

  function mergeField ( key ) {
    if ( lifecycles[ key ] ) {
      // 处理生命周期形成队列
      opts[ key ] = lifecycles[ key ]( preOptions[ key ], postOptions[ key ] )
    } else {
      // 优先使用后者
      opts[ key ] = postOptions[ key ] || preOptions[ key ]
    }
  }
  return opts
}


export function getContainer ( el ) {
  return typeof el === "string" ? document.querySelector( el ) : el;
}