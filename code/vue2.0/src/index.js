import { initMixin } from "./init";
import { initGlobalAPI } from "./initGlobalAPI";
import { lifeCycleMixin } from "./lifecycle";
import { compileToFunction } from "./compile/index";
import { createElm, patch } from "./vdom/patch";
function Vue ( options ) {
  this._init( options )
}

// 全局API
initGlobalAPI( Vue );
// 扩展原型方法
initMixin( Vue );
lifeCycleMixin( Vue )
export default Vue;


// ------------------头头对比----------------------
// const template1 = `<ul id='name'>
// <li style="background:red" key="A">A</li>
// <li style="background:yellow" key="B">B</li>
// <li style="background:green" key="C">C</li>
// </ul>`;

// const template2 = `<ul id='age'>
// <li style="background:purple" key="A">A</li>
// <li style="background:red" key="B">B</li>
// <li style="background:yellow" key="C">C</li>
// <li style="background:green" key="D">D</li>
// </ul>`;

// const template1 = `<ul id='name'>
// <li style="background:red" key="A">A</li>
// <li style="background:yellow" key="B">B</li>
// <li style="background:green" key="C">C</li>
// <li style="background:red" key="D">D</li>
// </ul>`;

// const template2 = `<ul id='age'>
// <li style="background:purple" key="A">A</li>
// <li style="background:red" key="B">B</li>
// <li style="background:yellow" key="C">C</li>
// </ul>`;

// ------------------尾尾对比----------------------

// const template1 = `<ul id='name'>
// <li style="background:red" key="C">C</li>
// <li style="background:yellow" key="B">B</li>
// <li style="background:green" key="A">A</li>
// </ul>`;

// const template2 = `<ul id='age'>
// <li style="background:green" key="D">D</li>
// <li style="background:yellow" key="C">C</li>
// <li style="background:red" key="B">B</li>
// <li style="background:purple" key="A">A</li>
// </ul>`;

// const template1 = `<ul id='name'>
// <li style="background:green" key="D">D</li>
// <li style="background:red" key="C">C</li>
// <li style="background:yellow" key="B">B</li>
// <li style="background:green" key="A">A</li>
// </ul>`;

// const template2 = `<ul id='age'>
// <li style="background:yellow" key="C">C</li>
// <li style="background:red" key="B">B</li>
// <li style="background:purple" key="A">A</li>
// </ul>`;

// ------------------老头新尾对比----------------------

// const template1 = `<ul id='name'>
// <li style="background:green" key="A">A</li>
// <li style="background:yellow" key="B">B</li>
// <li style="background:red" key="C">C</li>

// </ul>`;

// const template2 = `<ul id='age'>
// <li style="background:green" key="D">D</li>
// <li style="background:yellow" key="C">C</li>
// <li style="background:red" key="B">B</li>
// <li style="background:purple" key="A">A</li>
// </ul>`;

// const template1 = `<ul id='name'>
// <li style="background:green" key="A">A</li>
// <li style="background:yellow" key="B">B</li>
// <li style="background:red" key="C">C</li>

// </ul>`;

// const template2 = `<ul id='age'>
// <li style="background:yellow" key="C">C</li>
// <li style="background:purple" key="A">A</li>
// </ul>`;

// ------------------老尾新头对比----------------------

// const template1 = `<ul id='name'>
// <li style="background:yellow" key="B">B</li>
// <li style="background:red" key="C">C</li>
// </ul>`;

// const template2 = `<ul id='age'>
// <li style="background:yellow" key="C">C</li>
// <li style="background:purple" key="A">A</li>
// </ul>`;

// ------------------乱序对比复用最大化----------------------

// const template1 = `<ul id='name'>
// <li style="background:green" key="A">A</li>
// <li style="background:yellow" key="B">B</li>
// <li style="background:red" key="C">C</li>
// </ul>`;

// const template2 = `<ul id='age'>
// <li style="background:yellow" key="D">D</li>
// <li style="background:green" key="A">A</li>
// <li style="background:yellow" key="B">B</li>
// <li style="background:purple" key="E">E</li>
// </ul>`;

// const render1 = compileToFunction( template1 );
// const vm1 = new Vue( { data: {} } );
// let oldVnode = render1.call( vm1 );
// const el1 = createElm( oldVnode );
// document.body.appendChild( el1 );



// const render2 = compileToFunction( template2 );
// let newVnode = render2.call( vm1 );
// setTimeout( () => {
//   patch( oldVnode, newVnode );
// }, 1000 );
