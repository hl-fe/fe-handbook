---
title: 第二章｜HTML中的JavaScript
description: 记录JavaScript高级程序设计的学习过程
---

## script元素

- script元素可以包含在head元素中，也可以包含在body元素中，默认情况下是script`同步加载`、`同步执行`，等待执行完毕后，后面的才能开始执行

## script src遇上内联代码

- 如果有script标记有src属性，script开始到结束标签`内部的代码不会执行`

```js
// demo.js
console.log("demo");

<script src="demo.js">
  console.log("Hello World!");
</script>

// 输出结果：demo
```

## script异步加载

- defer属性，可以让script元素`异步加载`、加载完成后`不会立马执行`，这个脚本会等待`整个页面解析之后`在运用，并且在`DOMContentLoaded`事件之前执行，如有多个defer加载的脚本会按照`加载顺序依次执行`

```js
// 比如说example1.js加载5秒种，example2.js加载2秒钟
// example2会等待example1加载完并且执行后，在执行example2
<script defer src="example1.js"></script>
<script defer src="example2.js"></script>
```
- async属性，可以让script元素`异步加载`、加载完成后`立马执行`这个脚本，如有多个async加载的脚本无法保证他们的执行顺序

```js
// 如有多个async加载的脚本无法保证他们的执行顺序
<script async src="example1.js"></script>
<script async src="example2.js"></script>
```

## noscript标签

- noscript标签，当浏览器不支持script标签时，会显示noscript标签内的内容
