---
title: 第一章｜什么是JavaScript
description: 记录JavaScript高级程序设计的学习过程
---

## JavaScript实现包括

- 核心：ECMAScript
- 文档对象模型：DOM
- 浏览器对象模型：BOM

## ECMAScript

- ECMAScript是一种由Ecma国际（前身为欧洲计算机制造商协会）制定的脚本程序设计语言标准，是一种与JavaScript相对应的语言标准，是一种`独立于浏览器的脚本语言`，是一种`动态类型`、`弱类型`、`基于原型`的语言，支持多线程、面向对象、动态加载、正则表达式、错误处理、循环、日期和时间、函数库、DOM操作、

## DOM

- DOM（Document Object Model）文档对象模型，是W3C组织推荐的处理`网页内容的标准编程接口`，它是一个`树形结构`，它包含了`页面的所有元素`，通过它可以访问和操作页面的内容，比如获取页面的标题、获取页面的元素、获取页面的样式等。

## BOM

- BOM（Browser Object Model）浏览器对象模型，它是W3C组织推荐的`处理浏览器内容的标准接口`，比如`移动`、`缩放`和`关闭`浏览器，
  - navigator：用于获取浏览器的相关信息，比如浏览器的名称、版本、操作系统、浏览器的渲染引擎等。
  - location：用于获取当前页面的URL地址。
  - screen：用于获取屏幕的相关信息，比如屏幕的宽度、高度、颜色深度等。
  - performance：用于获取浏览器的性能信息，比如浏览器的加载时间、页面的渲染时间等。
  - 其他自定义对象如：`XMLHttpRequest`和IE的`ActiveXBoject`对象。
