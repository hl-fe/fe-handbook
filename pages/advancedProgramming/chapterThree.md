---
title: 第三章｜语言基础
description: 记录JavaScript高级程序设计的学习过程
---

## var关键字

- 用`var`关键字声明的变量，可以存储任何类型的值(在不初始化的情况下，会存储一个特殊的值`undefined`)

### var的作用域

- 1、在全局环境中用`var`声明的变量是不会被销毁的

```js
var test = 10;
function fn(){
  console.log(test); // 10
}
console.log(test); // 10
```

- 2、在函数中用`var`声明的变量，属于`局部变量`，只在函数内部有效，在函数外部是不可见的，函数执行完毕后，函数内部的变量会被`销毁`

```js
function fn(){
  var test = 10;
  console.log(test); // 10
}
console.log(test); // Uncaught ReferenceError: test is not defined

// 在函数内部如果有一个变量，没有用关键字声明函数执行完毕后，挂载到全局环境中
function fn(){
  test = 10;
}
```

### 3、var变量提升

- 1、`var`声明的变量会被提升到当前作用域的最顶部

```js
function fn(){
  console.log(test); // undefined
  var test = 10;
}
```

- 2、`var`声明的变量会被提升到`当前作用域的最顶部`，但是不会提升到`全局作用域`

```js
function fn(){
  console.log(test); // undefined
  var test = 10;
}
```

## let关键字

- 1、`let`声明的变量，在当前的代码块中有效

```js
{
  let test = 10;
}
console.log(test); // ReferenceError: test is not defined
```

- 2、变量不会存在类型提升

```js
function test() {
  console.log(a);
  let a = 10;
}
test() // ReferenceError: Cannot access 'a' before initialization
```

- 3、不允许重复声明

```js
function test() {

  let a = 10;
  let a = 11; // SyntaxError: Identifier 'a' has already been declared
}
```
- 4、存在暂时性死区

- javascript在解析代码的时候，如果有let声明的变量，并且声明之前访问，就会报错，这就是暂时性死区

```js
function test() {
  console.log(a);
  let a = 10;
}
test() // ReferenceError: Cannot access 'a' before initialization
```


- 5、let在全局作用域中声明的变量，不会成为window对象的属性

```js
// 浏览器环境下
let age = 10;

console.log(window.age); // undefined

```

## const关键字

- 1、`const`声明的变量，在声明的时候必须初始化，不能在声明之后再次赋值

```js
const a = 10;
a = 11; // TypeError: Assignment to constant variable.
```

- 2、如果用`const`声明的变量指向一个引用地址，可以改变这个引用地址的值，但是不能改变引用地址本身

```js
const a = {
  name: 'test'
};
a.name = 'test1';
console.log(a.name); // test1
```
## const、let、var最佳实践

- 尽量使用`const`，只有在必要的时候才使用`let`，尽量不要使用`var`

## 数据类型

- ECMAScript一共有6种简单的数据类型分别是：Undefined、Null、Boolean、Number、String、Symbol（ES6新增），还有一种复杂数据类型是Object（对象）。Object是无序的，可以包含其他任意类型值的对象。

## Undefined类型

- 当使用了var或let关键字声明了变量，但是没有赋值的时候，会默认赋值为undefined


```js
let name;
console.log(name==undefined);// true
```
::: warning
注意：包含了undefined的值的变量根未定义变量的区别

无论是声明未赋值的，还是未声明的typeof都是undefined
:::

```js
let name;
console.log(name); // "undefined"
console.log(typeof age); // "undefined"
console.log(age); // Uncaught ReferenceError: a is not defined
```

## Null类型

- Null类型是一个特殊的值，表示一个空对象引用`(空指针)`，常用于表示`对象`未初始化或为空值。
- 在JavaScript最初的实现中，数据转成二进制后判断前2位如果是0,就代表是一个对象，null是一个空的地址，所有位都是0，所以typeof null 也因此返回 "object"

```js
console.log(typeof null); // "object"
console.log(null == undefined); // true
```

## Boolean类型

- Boolean类型只有两个值：`true`和`false`。
- 注意：在JavaScript中，`0`、`-0`、`""`、`null`、`undefined`、`NaN`都会被转换为`false`，其他值都会被转换为`true`。

## Number类型

- Number类型是浮点数，JavaScript的浮点数采用IEEE 754标准，这个标准定义了浮点数的表示方法，数值的表示范围，运算方式。
- 浮点数在运算过程中会产生误差，因为计算机无法精确表示无限循环小数。

### Number的范围
- Number的范围是`-2^53 ~ 2^53`，超过这个范围，就会变成`Infinity`，小于这个范围，就会变成`-Infinity`。
- 无穷大`Infinity`和`-Infinity`可以相加，得到`NaN`。

### NaN
- NaN表示非数值（Not a Number），`NaN`与任何值都不相等，包括它本身。
- `NaN`与任何值包括它自己都不相等，包括`NaN`本身。
- `isNaN()`函数可以判断一个值是否是`NaN`。
- 0,+0,-0 相除会返回NaN

### 如果分子是非0值，分母是有符号0或无符号0，则会返回Infinity或-Infinity

```js
console.log(1/0); // Infinity
console.log(1/-0); // -Infinity
```

### 数值转换
- 布尔值true转换为1，false转换为0。
- 数值直接返回
- null返回0
- undefined返回NaN
- 字符串转换为数值，如果字符串不能转换为数值，则返回NaN。
- 对象转换为数值，首先调用对象的valueOf()方法，如果返回原始类型的值，则直接对该值进行转换。如果valueOf()方法返回的是对象，则调用该对象的toString()方法，将得到的字符串转换为数值。

```js
console.log(Number(true)); // 1
console.log(Number(false)); // 0
console.log(Number(1)); // 1
console.log(Number(undefined)); // NaN
console.log(Number('1')); // 1
console.log(Number('10a')); // NaN
console.log(Number({a:10}));// NaN
console.log(Number({valueOf(){return 10}})); // 10
```

## String类型

| 字面量 |            含义                   | 
| :--: | :-------------------------------: | 
| \n | 换行 | 
| \t | 制表|
| \b | 退格 |
| \r | 回车 |
| \f | 换页 |
| \\\ | 反斜杠|
| \\' | 单引号 |
| \\" | 双引号 | 

### toString()数值二进制、八进制、十六进制转换

```js
var num = 10;
console.log(num.toString()); // "10" 默认十进制
console.log(num.toString(2)); // "1010"
console.log(num.toString(8)); // "12"
console.log(num.toString(10)); // "10"
console.log(num.toString(16)); // "a"
```

## Symbol类型
- Symbol类型是ES6新增的类型，用来创建唯一标识符。
- Symbol函数可以接受一个字符串作为参数，表示对Symbol实例的描述，主要是为了在控制台显示，或者转为字符串时，比较容易区分。
- Symbol函数的参数只是表示对当前Symbol值的描述，因此相同参数的Symbol函数的返回值是不相等的。

```js
let s1 = Symbol('foo');
let s2 = Symbol('bar');
console.log(s1); // Symbol(foo)
console.log(s2); // Symbol(bar)
console.log(s1 === s2); // false
new Symbol('foo') // TypeError: Symbol is not a constructor

```

### Symbol.for()和Symbol.keyFor()

- Symbol.for()方法接受一个字符串作为参数，然后搜索有没有以该参数作为名称的Symbol值。如果有，就返回这个Symbol值，否则就新建并返回一个以该字符串为名称的Symbol值。
- Symbol.keyFor()方法返回一个已登记的Symbol类型值的key。

```js
let s1 = Symbol.for('foo');
let s2 = Symbol.for('foo');
let s3 = Symbol.for('bar');
console.log(s1 === s2); // true
console.log(Symbol.keyFor(s1)); // "foo"
```

## Object类型
- Object类型是JavaScript中最复杂的类型，它表示一个对象，对象是由属性和属性值组成的。
- 对象的每个属性或方法都有一个名字，而每个名字都对应一个值。
- 属性的值可以是任意数据类型，包括对象。
- new Object() 等价于 new Object
