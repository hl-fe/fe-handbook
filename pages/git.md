# Git 管理

## Git 分支命名规范

- master 永远是可用的稳定版本
- develop(dev) 正在测试但未上线的版本
- feature(feat)/xxx 功能和特性版本
- hotfix(fix)/xxx 紧急的 Bug

## Git commit 规范

- feat: 新增 feature
- fix: 修复 bug
- docs: 仅仅修改了文档，如 readme.md
- style: 仅仅是对格式进行修改，如逗号、缩进、空格等。不改变代码逻辑。
- refactor: 代码重构，没有新增功能或修复 bug
- perf: 优化相关，如提升性能、用户体验等。
- test: 测试用例，包括单元测试、集成测试。
- chore: 改变构建流程、或者增加依赖库、工具等。
- revert: 版本回滚

## Git 基本命令

- git 基本命令
- git add <文件> 添加暂存
- git status -s 查看文件暂存状态
- git commit -m “注释” 需要暂存,提交
- git commit -am “注释” 不需要暂存,直接提交
- git push 上传本地文件到远程
- git pull 拉取远程到本地
- git push origin dev 本地分支上传的远程
- git push origin --delete dev 删除远程分支
- git branch 查看本地分支
- git branch -D dev 删除本地分支
- git branch -a //查看本地远程分支
- git branch feature/xxx 创建新分支
- git checkout feature/xxx //切换分支
- git fetch 更新拉取分支
- git log -p -10 查看提交记录
- git diff 可以查看当前没有 add 的内容修改（不在暂存的文件变化）
- git diff --cached 查看已经 add 但没有 commit 的改动（在暂存的文件变化）
- git diff HEAD 是上面两条命令的合并
- git reset HEAD src/xx/xx 取消暂存
- git checkout src/xx/xx 重置修改的文件(需要取消暂存)
- git merge hotfix 合并分支
- git reset --hard commit-id :回滚到 commit-id
- git reset --hard HEAD~3：将最近 3 次的提交回滚
- git fetch --all 拉取最新的
- git reset --hard origin/master 用远程覆盖本地
- git stash 拉别人代码前暂存
- git stash pop 释放暂时
- git rm -r --cached components.d.ts 忽略已经提交的文件

## Git 版本回退

- 1. git log
     > commit id 和 commit message

- 2. git reset --hard id
     > 根据 commit id 回退到指定的版本(退回这个版本，废除这次提交)

- 3. git push origin --force

     > 推送到本地到远程仓库：让远程仓库代码和你本地一样，到当前你本地的版本。

     > 这个时候突然又发现不需要回退了，刚才那些消失的代码又要重新找回来了，别担心，咱们 Git 强大着呢！

- 4. git reflog

     > 定义：查看命令操作的历史

     > 查找到你要的 操作 id，依旧使用 上文说的 git reset --hard id。又回退到当初一模一样的版本啰！

## Git commit 后如何撤销

> 不删除工作空间修改过的代码，仅撤销 commit

- git reset --soft HEAD^

> 删除工作空间修改过的代码，撤销 commit&撤销 add

- git reset --hard HEAD^

## Git冲突后取消merge

> 分支merge有冲突，取消merge

- git merge --abort

## git取消记录合并

- 1. git log
  > commit id 和 commit message

- 2. git revert commitId
  > 使用git revert撤销合并

## git取消某次合并

- 1. git log
  > commit id 和 commit message

- 2. git reset commitId
  > 使用git reset取消合并

## 查看当前分支是从哪个分支checkout出来的

 - git reflog show --date=local | grep 分支名称
  > checkout: moving from master to 16069  16069分支是从mastercheckout出来的

## git中rebase和merge的区别是什么

- 1、rebase把当前的commit放到公共分支的最后面，merge把当前的commit和公共分支合并在一起
- 2、用merge命令解决完冲突后会产生一个commit，而用rebase命令解决完冲突后不会产生额外的commit

## Git 分支 A 方案版

![eventLoop](/images/git-a.png)

## Git 分支 B 方案版

![eventLoop](/images/git-b.png)