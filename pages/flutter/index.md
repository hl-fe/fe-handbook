# flutter

## 数据类型

### 数字类型

```js
 num num1 = -100; // 是数字类型的父类，有两个子类 int 和 double。
 num num2 = 100;
 int int1 = 100; //只能是整数
 double d1 = 3.1415; //双精度
 num1.abs();  //求绝对值
 num1.toInt(); //转换成int
 num1.toDouble(); //转换成Double
```

### 字符串

```js
String str1 = '单引号';
String str2 = "双引号";
String str3 = 'st1:$str1 str2:$str2'; //字符串拼接
String str4 = 'st1:' + str1 + ' str2:' + str2; //字符串拼接
//常用方法
print(str5.substring(1, 5)); //字符串截取
print(str5.indexOf('类型')); //获取指定字符串位置
```

### 布尔类型

- Dart 是强 bool 类型检查，只有 bool 类型的值是 true 才被认为是 true

```js
bool success = true;
bool fail = false;
```

### List 集合

```js
List list = [1, 2, 3, '集合']; //初始化时，添加元素

List<int> list2 = []; // 类型约束

List list3 = [];
list3.addAll(list);// 直接添加整个集合
print(list3);

List list4 = List.generate(3, (index) => index * 2);
print(list4);

///遍历集合的方式
for (int i = 0; i < list.length; i++) {
  print(list[i]);
}
for (var o in list) {
  print(o);
}
list.forEach((val) {
  print(val);
});
```

### Map

```js
 Map names = {'username': 'hu.lei', 'age': 'hulei'};
names['add'] = 'sh'

///Map遍历
ages.forEach((k, v) {
  print('$k $v');
});
Map ages2 = ages.map((k, v) {
  //迭代生成一个新Map
  return MapEntry(v, k);
});
print(ages2);
for (var key in ages.keys) {
  print('$key ${ages[key]}');
}
```

## dynamic、var、Object 三者的区别

- dynamic
  - dynamic 是动态类型，不建议使用，因为代码静态类型检查不会报错
- var
  - var 是一个关键字,会自动类型推导，如果声明的是字符串，在赋值 int 类型，在编译时就会报错
- Object

```js
  dynamic a1 = 'hu.lei';
  print(a1.runtimeType); // String
  a1 = 20;
  print(a1.runtimeType);// int
  a1.fn() // 编译时不会报错，运用时才会报错
```

```js
var a1 = "hu.lei";
print(a1.runtimeType); // String
a1 = 20; //  Error: A value of type 'int' can't be assigned to a variable of type 'String'.
```

```js
Object o1 = '11';
print(o1.runtimeType); // String
print(o1.fn()); //  Error: The method 'fn' isn't defined for the class 'Object'.
```

## 面向对象

```js
// 实例化
Student student =Student('hu.lei',20,'东风之星',city:'北京');
student.school; // 调用get取值
student.school = '东风之星'; // 调用set赋值
Student.doPrint('调用静态方法');
print( student.toString());

// 工厂构造实例化
Student factoryStudent = Student.stu(Student('hu.lei',20,'东风之星',city:'北京'));
factoryStudent.school; // 调用get取值
factoryStudent.school = '东风之星'; // 调用set赋值
print( factoryStudent.toString());

// 子类
class Student extends Person{
  //定义类的变量
  String _school; //通过下划线来标识私有字段（变量) 只能在本类中使用
  String city;

  // city默认参数
  // 如果父类中有构造，必须调用父类的构造
  Student(String name,int age,String school, {this.city='上海'}):_school ='$name.$school',super(name, age) {
    print('构造方法体不是必须的');
  }
  // get取值
  String get school => _school;

  // get设置
  set school(String value) {
    _school = value;
  }

  //命名工厂构造方法：类名.方法名调用
  factory Student.stu(Student stu) {
    return Student(stu.name, stu.age, stu.school, city: stu.city);
  }

  @override
  String toString() {
    return '$name $age $_school $city';
  }

  //静态方法
  static doPrint(String str) {
    print('doPrint:$str');
  }
}

// 父类
class Person{
  String name;
  int age;
  // 有参数构造
  Person(this.name,this.age){
  }
  @override
  String toString() {
    return '$name $age';
  }
}

```

## 单例模式

- 是有一个实例对象

```js
// 单例
SingletonPattern singletonPattern = SingletonPattern();
SingletonPattern singletonPattern1 = SingletonPattern();
print(singletonPattern==singletonPattern1);
singletonPattern.test();

///工厂构造方法演示
class SingletonPattern {
  static final SingletonPattern _singletonPattern =
  SingletonPattern._internal();

  ///工厂构造函数
  factory SingletonPattern() {
    return _singletonPattern;
  }

  ///构造函数私有化，防止被误创建
  SingletonPattern._internal();

  void test() {
    print("测试");
  }
}
```

## 继承抽象类

- 被 abstract 修饰的类就是抽象类，抽象类中的方法不能有方法体
- 如果一个类继承一个抽象类，必须实现抽象类中的方法

```js
 class Dog extends Animal {
  @override
  void eat() {
    print('吃饭');
  }
}

abstract class Animal {
  void eat();
}
```

## with 关键字

- 类在继承中只能单继承
- 如果一个类中，需要继承多个类的特性，可以用 with

```js
class User extends BaseUser with User1,User2 {

}
```

## 泛型

- 泛型可以约束类型，可以进行类型推导

```js
Cache<String> cache = Cache();
cache.setCache('key1');
print(cache.getCache());

class Cache<T>{
  late T key;
  setCache(T key){
    this.key = key;
  }
  T getCache(){
    return this.key;
  }
}
```

## Widget 的生命周期

```js
import 'package:flutter/material.dart';

///Flutter Widget的生命周期重点讲解StatefulWidget的生命周期
///因为无状态的widget StatelessWidget只有createElement、与build两个生命周期方法
///StatefulWidget的生命周期方法按照时期不同可以分为三组：
///1.初始化时期
///createState、initState
///2.更新期间
///didChangeDependencies、build、didUpdateWidget
///3.销毁期
///deactivate、dispose
///扩展阅读：
///http://www.devio.org/io/flutter_app/img/blog/flutter-widget-lifecycle.png
///https://flutterbyexample.com/stateful-widget-lifecycle/
class WidgetLifecycle extends StatefulWidget {
  const WidgetLifecycle({super.key});


  ///当我们构建一个新的StatefulWidget时，这个会立即调用
  ///并且这个方法必须被覆盖
  @override
  State<WidgetLifecycle> createState() => _WidgetLifecycleState();
}

class _WidgetLifecycleState extends State<WidgetLifecycle> {
  int _count = 0;

  ///这是创建widget时调用的除构造方法外的第一个方法：
  ///类似于Android的：onCreate() 与iOS的 viewDidLoad()
  ///在这个方法中通常会做一些初始化工作，比如channel的初始化，监听器的初始化等
  @override
  void initState() {
    print('----initState----');
    super.initState();
  }

  ///当依赖的State对象改变时会调用：
  ///a.在第一次构建widget时，在initState（）之后立即调用此方法；
  ///b.如果的StatefulWidgets依赖于InheritedWidget，那么当当前State所依赖InheritedWidget中的变量改变时会再次调用它
  ///拓展：InheritedWidget可以高效的将数据在Widget树中向下传递、共享，可参考：https://book.flutterchina.club/chapter7/inherited_widget.html
  @override
  void didChangeDependencies() {
    print('---didChangeDependencies----');
    super.didChangeDependencies();
  }

  ///这是一个必须实现的方法，在这里实现你要呈现的页面内容：
  ///它会在在didChangeDependencies()之后立即调用；
  ///另外当调用setState后也会再次调用该方法；
  @override
  Widget build(BuildContext context) {
    print('---build-----');
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter页面生命周期'),
        leading: BackButton(),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            ElevatedButton(
              onPressed: () {
                setState(() {
                  _count += 1;
                });
              },
              child: const Text(
                '点我',
                style: TextStyle(fontSize: 26),
              ),
            ),
            Text(_count.toString())
          ],
        ),
      ),
    );
  }

  ///这是一个不常用到的生命周期方法，当父组件需要重新绘制时才会调用；
  ///该方法会携带一个oldWidget参数，可以将其与当前的widget进行对比以便执行一些额外的逻辑，如：
  /// if (oldWidget.xxx != widget.xxx)...
  @override
  void didUpdateWidget(WidgetLifecycle oldWidget) {
    print('----didUpdateWidget-----');
    super.didUpdateWidget(oldWidget);
  }

  ///很少使用，在组件被移除时调用在dispose之前调用
  @override
  void deactivate() {
    print('-----deactivate------');
    super.deactivate();
  }

  ///常用，组件被销毁时调用：
  ///通常在该方法中执行一些资源的释放工作比如，监听器的卸载，channel的销毁等
  @override
  void dispose() {
    print('-----dispose-----');
    super.dispose();
  }
}


```

## APP 生命周期

```js
import 'package:flutter/material.dart';

class AppLifecycle extends StatefulWidget {
  const AppLifecycle({Key? key}) : super(key: key);

  @override
  _AppLifecycleState createState() => _AppLifecycleState();
}

class _AppLifecycleState extends State<AppLifecycle>
    with WidgetsBindingObserver {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter应用生命周期'),
        leading: const BackButton(),
      ),
      body: const Text('Flutter应用生命周期'),
    );
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    print('state = $state');
    if (state == AppLifecycleState.paused) {
      print('App进入后台');
    } else if (state == AppLifecycleState.resumed) {
      print('App进去前台');
    } else if (state == AppLifecycleState.inactive) {
      //不常用：应用程序处于非活动状态，并且未接收用户输入时调用，比如：来了个电话
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
}

```

## flutter sdk 版本

- stable（推荐）：为当前最稳定的 channel
- beta：为每月 Flutter 官方调整选出来的最好的 dev 的 channel；
- dev：为当前最新且充分测试后的 channel；
- master：为当前最新的 channel；

```npm
# 查看当前Flutte渠道
flutter channel

# 切换Flutte渠道
flutter channel dev

# 更新当前Flutte渠道
flutter upgrade
```
