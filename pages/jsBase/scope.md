# 闭包

## 什么是闭包
- 闭包：函数运行的一种机制
- 函数执行会形成一个`私有上下文`，如果上下文中的某些`变量`(一般指的是堆内存地址)`被上下文以外的一些事物`所引用，导致当前上下文不能被出栈释放形成闭包

- 保护：保护私有上下文中的“私有变量”和外界互不影响
- 保存：上下文不被释放，那么上下文中的”私有变量”和”值”都会被保存起来，可以供其下级上下文中使用
- 弊端：如果大量使用闭包，会导致栈内存太大，页面渲染变慢，性能受到影响，所以真实项目中需要“合理应用闭包”；某些代码会导致栈溢出或者内存泄漏，这些操作都是需要我们注意的；


- 1、把函数作为参数传递
- 2、返回一个函数

```js
// 返回一个函数
function closure1() {
  let a = 10;
  return () => {
    console.log(a);
  };
}

// 把函数作为参数传递
function closure2(fn) {
  let a = 20;
  fn();
}
let a = 30;
function fn() {
  console.log(a);
}

closure1()();
closure2(fn);
```

## 闭包使用场景

- 防抖、截流函
- Vue2 响应式 dep
- react Hooks

## 闭包的内存泄漏

- 闭包一般情况不会造成内存泄漏，但是闭包内的引用是无法被垃圾回收的。

