# cookie&local

## cookie的介绍

- cookie最初的设计是来弥补http设计的不足，因为http是无状态的协议，同一用户2次访问一个页面，服务器无法识别，所以才引入cookie
- cookie是存储在客户端中，每次访问,就会自动携带上cookie信息
- cookie是不可以跨域的，每个cookie都会绑定单一的域名（绑定域名下的子域都是有效的），无法在别的域名下获取使用，同域名不同端口也是允许共享使用的。

## cookie访问限制

- domain`域`:在默认情况下domain是当前域名
- path：默认情况下的是根`/`
- 过期时间：默认情况下是会话级别的，浏览器关闭后就删除了
- cookie大小4kb

## cookie-domain

- 比如在`www.baidu.com/hulei/applyfor` 设置cookie 默认在`domain=www.baidu.com`域下，`aa.baidu.com`域下是无法访问的
- 如果domain设置为`.baidu.com` ,`aa.baidu.com`域可以访问


## cookie-path
- 比如在`www.baidu.com/hulei/applyfor` 设置cookie 默认在`path=/`,`www.baidu.com`域下都可以访问
- 如果path设置为`/applyfor`,只有在`applyfor以及applyfor/xx`后的才能访问

## cookie-属性

|  属性   | 说明                                   |
|  ----  | -------------------------------------  |
| name=value  | 设置值                             |
| domain  | 指定 cookie 所属域名，默认是当前域名       |
| path  | 指定 cookie 在哪个路径（路由）下生效，默认是 '/'。如果设置为 /abc，则只有 /abc 下的路由可以访问到该 cookie，如：/abc/read。 |
| expires  | 过期时间，默认不设置关闭浏览器删除 |
| max-age  | cookie 有效期，单位秒，优先级高于 expires  |
| HttpOnly  | 如果给某个 cookie 设置了 httpOnly 属性，则无法通过 JS 脚本 读写该 cookie 的信息，但还是能通过 Application 中手动修 cookie，所以只是在一定程度上可以防止 CSRF 攻击，不是绝对的安全 |
| secure  | 该 cookie 是否仅被使用安全协议传输。安全协议有 HTTPS，SSL等，在网络上传输数据之前先将数据加密。默认为false。当 secure 值为 true 时，cookie 在 HTTP 中是无效的。 |

## localStorage、sessionStorage

- localStorage 只要在相同的协议、相同的主机名、相同的端口下，就能读取/修改到同一份 localStorage 数据。5M(不需要在同一个窗口)
- sessionStorage 比 localStorage 更严苛一点，除了协议、主机名、端口外，还要求在同一窗口（也就是浏览器的标签页）5M
 
```js
// 打开一个新tab标签，加上opener可以复制sessionStorage，修改不会共享
<a href="./other.html" target="_blank" rel="opener">
  打开新页面
</a>
```