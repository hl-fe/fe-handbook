# 正则

## 词量修饰符，设置出现的次数
- 1） * 零到多次
- 2）+ 一到多次
- 3）？零次或者一次
- 4）{n} 出现n次
- 5）{n,} 出现n到多次
- 6）{n,m} 出现n到m次

## 匹配

- 1）\ 转义字符
- 2）. 除\n(换行符) 以外的任意字符
- 3）^ 以哪一个字符作为开始
- 4）$ 以哪一个字符作为结束
- 5）\n 换行符
- 6）\d 0-9
- 7）\D 非0-9
- 8）\w 数字、字母、下划线
- 9）\s 空白字符
- 10）\t 一个制表符
- 11）\b 匹配一个单词的边界
- 12）x|y 匹配x或者y
- 13) [zyc] 匹配x或者y或者c
- 14) [^ab] 除了ab
- 15) () 分组
- 16)  (?:)只匹配不捕获
- 17)  (?=) 正向预查
- 8)  (?!) 负向预查

## exec

- 全部模式下也只会匹配一次，需要手动设置lastIndex
```js
let reg = /(\d+)/g;
console.log(reg.exec("2020-12-11")); // 2020

reg.lastIndex = 4;
console.log(reg.exec("2020-12-11")); // 12

reg.lastIndex = 7;
console.log(reg.exec("2020-12-11")); // 11

console.log("2020-12-11".match(reg)); // [ '2020', '12', '11' ]
```

## match(全部模式下会匹配所有)

```js
console.log("2020-12-11".match(reg)); // [ '2020', '12', '11' ]
```

## 中括号中不存在多位数[189] => [1|8|9]

```js
let reg = /^[189]$/;
console.log(reg.test(1)); // true
console.log(reg.test(8)); // true
console.log(reg.test(9)); // true
console.log(reg.test(189)); // false
```

## 手机号码

```js
let reg = /^1[3456789]d{9}$/
console.log(reg.test(17624346544));
```

## 最多2位小数

- 1) \d+ 只能是数字
- 2）(\.\d{1,2})小数位后只能是一到2位数字 ? 0到一次

```js
let reg = /^\d+(\.\d{1,2})?$/;
console.log(reg.test(10));
console.log(reg.test(10.2));
console.log(reg.test(10.22));
console.log(reg.test(10.222));
```

## 中文姓名

```js
let reg = /^[\u4E00-\u9FA5]{2}$/;
console.log(reg.test("胡额"));
```

## 身份证

```js
let reg = /^(\d{6})(\d{4})(\d{2})(\d{2})(\d{2})(\d)(\d|X|x)$/;
console.log(reg.exec("420881199411298114"));
```

## 正则贪婪模式(最可能找最大的匹配)

```js
let reg = /\d+/g;
console.log("2019@2020".match(reg)); // [ '2019', '2020' ]
```

## 正则取消贪婪模式

```js
let reg = /\d+?/g;
console.log("2019@2020".match(reg)); // ['2', '0', '1','9', '2', '0','2', '0']
```

## 计算出现最多的字母

- \1 匹配连续出现 [a-zA-Z])\1 aa  [a-zA-Z])\1{1} aa  [a-zA-Z])\1{2} aaa
- 1) 字符先排序
- 2）遍历字符从索引结尾开始
- 3）正则匹配连续的字符
- 4）匹配到就终止循环

```js
let str = "jabaambbjbcha";
str = str
  .split("")
  .sort((a, b) => a.localeCompare(b))
  .join("");
let flog = false;
let max = 0;
let arr = [];
for (let index = str.length; index > 0; index--) {
  if (flog) break;
  var reg = new RegExp(`([a-zA-Z])\\1{${index - 1}}`, "g");
  str.replace(reg, (content, $1) => {
    max = content.length;
    arr.push($1);
    flog = true;
  });
}
console.log(max, arr);
```

## 首字母大写

```js
let str = "after a while , charley lowered his head . he laughed and asked";
str = str.replace(/\b([a-zA-Z]+)\b/g, (content) => {
  return content[0].toUpperCase() + content.substring(1);
});
console.log(str);
```

## 日期格式化

```js
function formatTime(template = "{0}年{1}月{2}日 {3}时{4}分{5}秒") {
    let arr = this.match(/(\d+)/g);
    return template.replace(/\{(\d+)\}/g, ([, $1]) => {
      time = arr[$1] || "00";
      return time.length === 1 ? "0" + time : time;
    });
  }
```

## 获取url参数

```js
 function queryUrlParams() {
    let res = {};
    this.replace(/([^?=&#]+)=([^?=&#]+)/g, (...[, k, v]) => (res[k] = v));
    this.replace(/#([^?=&#]+)/g, (...[, v]) => (res["hash"] = v));
    return res;
  }

```

## 正则数组去重

```js
let arr = [1, 23, 43, 1, 45, 33, 45, 11, 33];
let str = arr.sort((a, b) => a - b).join("@") + "@";
arr = [];
str.replace(/(\d+@)\1*/g, (content, $1) => {
  arr.push($1.replace("@", ""));
});
console.log(arr);

```

## 则实现IndexOf
- match 匹配到会返回当前字符的位置，未找到返回null

```js
String.prototype.myIndexOf = function (str) {
  let reg = new RegExp(str);
  let res = this.match(reg);
  return res === null ? -1 : res["index"];
};
console.log("aabbccb".indexOf("bbccb")); // 2
console.log("Blue Whale".indexOf("Blue")); // 0
console.log("Blue Whale".indexOf("Blute")); // -1
console.log("sfsdfsgssgdsgsdh".indexOf("h")); // 15
console.log("-----------------------");
console.log("aabbccb".myIndexOf("bbccb")); // 2
console.log("Blue Whale".myIndexOf("Blue")); // 0
console.log("Blue Whale".myIndexOf("Blute")); // -1
console.log("sfsdfsgssgdsgsdh".myIndexOf("h")); // 15
```

## 匹配出来的hello字符串后边要跟着3个数字

```js
var reg = /\d{1,3}(?=\d{3})/g;
var str = "123456789";
str.replace(reg, (content, a, b) => {
  console.log(content, a, b);
});

```

## 实现数组each 函数
- 1) 如何返回false 终止循环
- 2）如果返回其他，则改变原数组的值

```js
Array.prototype.each = function (callback, self) {
  if (Object.prototype.toString.call([]) !== "[object Array]")
    return new Error("no is array");
  const content = self || this;
  for (let index = 0; index < this.length; index++) {
    let result = callback.call(content, index, this[index]);
    if (result === false) {
      break;
    }
    if (result) {
      this[index] = result;
    }
  }
};

let arr = [1, 2, 3, 4];
arr.each(function (index, item) {
  console.log(index, item);
  if (item === 2) {
    return 20;
  }
});

console.log(arr);
```

## 字母取反Abc aBC

```js
let str = "Abc";
let newStr = str.replace(/([a-zA-Z])/g, (content) => {
  const tempStr = content.toLocaleUpperCase();
  return tempStr === content ? content.toLocaleLowerCase() : tempStr;
});
console.log(newStr);

```

## url 验证

- (?:(http|https|ftp):\/\/)? 0次或者1次，只匹配不捕获
- ((?:[\w]+\.)+[a-z0-9]+) xx.xx
- ((?:\/[^/?#]*)+)?
- (\?[^#]+)?
- (#.+)?

```js
let reg = /^(?:(http|https|ftp):\/\/)?((?:[\w]+\.)+[a-z0-9]+)((?:\/[^/?#]*)+)?(\?[^#]+)?(#.+)?$/i;

console.log(reg.exec("https://www.baidu.com/index.html?age=10#home"));
console.log(reg.test("baidu.com"));
console.log(reg.test("https://www.baidu.com"));
console.log(reg.test("http://www.baidu.com.cn"));
console.log(reg.test("http://www.baidu.com.cn.net"));
```

## 同时满足字母大小写和数字

- 反向预查
- (?!^[a-zA-Z]+$) 不能是全字母
- (?!^[0-9]+$) 不能是全数字
- (?!^[a-z0-9]+$) 不能是小写字母+数字
- (?!^[A-Z0-9]+$) 不能是大写字母+数字
- ^[a-zA-Z0-9]{3,}$ 只能是数字字母

```js
let reg =/(?!^[a-zA-Z]+$)(?!^[0-9]+$)(?!^[a-z0-9]+$)(?!^[A-Z0-9]+$)^[a-zA-Z0-9]{3,}$/;

console.log(reg.test("aaab"));
console.log(reg.test("aaBva"));
console.log(reg.test("11111144"));
console.log(reg.test("adv44"));
console.log(reg.test("AA44"));
console.log(reg.test("aB4"));
```

## 必须有下划线

```js
let reg = /(?!^[a-z0-9A-Z]+$)^\w{6,10}$/;
console.log(reg.test("ssssdfd_"));
console.log(reg.test("_ssssfd_"));
console.log(reg.test("_sssfd_s11"));


let reg = /(?=_)\w+/;
console.log(reg.test("_ssssdfd_"));
console.log(reg.test("_ssssfd_"));
console.log(reg.test("_sssfd_s11"));

```

## userName => user_name

```js
let userName = "userName";
userName = userName.replace(/([A-Z])/g, (a) => `_${a.toLocaleLowerCase()}`);
console.log(userName);

```

## user_name => userName

```js
let user_name = "user_name";
user_name = user_name.replace(/_(\w)/g, ([, $2]) => `${$2.toUpperCase()}`);
console.log(user_name);

```