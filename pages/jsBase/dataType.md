# js数据类型

## 数据类型种类

:::tip
JS 中有8种数据类型
:::

- 七种基本数据类型 `Boolean` `Null` `Undefined` `Number` `String` `Symbol` `bigInt`
- 一种引用类型 `object` `{} [] /^$/ new Date() Math function`

## typeof关键字
- typeof 返回的都是字符串

```js
typeof a // undefined
typeof 1 // number
typeof NaN // number
typeof "zhufeng"; // string
typeof true; // boolean
typeof Symbol("a"); // symbol
typeof function () {}; //function
typeof [1, 2, 3]; //object
typeof { name: "zhufeng" }; //object
typeof new Number(1); //object
typeof null; //object
```

## typeof null 等于object，为什么不修复？
- javascript 在设计之初的遗留问题
- 因为目前很多程序、 js 库都有这个逻辑判断、修复后会给程序带来很多问题

## null 和 undefined 区别？

- null 和 undefined 都代表空，主要区别在于 undefined 表示`尚未初始化的变量的值`，而 null 表示该变量指向`空地址`。
- 在加法操作时，隐式类型转换不同，`null会转换成0` `undefined会转换成NaN`

```js
var a 
console.log(a);// undefined
var b = null

console.log(+undefined) // NaN
console.log(+null) // 0
```

## 引用类型
- 引用类型是指向一个内存地址
:::warning
如果有赋值关系，当其中一个属性发生变化，会影响其他的值
:::


```js
let a = {username:'hulei'};
let b = a;
b.age = 20
console.log(b) // {username:'hulei',age:20}
```

![引用类型](/images/01.png)


## 内存区域

- 程序运行的时候，需要内存空间存放数据。一般来说,系统会划分出两种不同的内存空间：一种叫做 stack(栈)，另一种叫做 heap(堆)
  - 1、stack(栈)是有结构的，每个区块按照一定次序存放，可以明确知道每个区块的大小
  - 2、 heap(堆)是没有结构的，数据可以任意存放。因此，stack 的寻址速度要快于 heap

- 基本数据类型，一般都存放在 stack 里面，对象引用类型 一般都存放在 heap 里面


##  var&let&const

- `var` 定义的变量`没有块的概念`,`可以跨块访问`,`不能跨函数访问`,`有变量提升`,`可重复声明`
- `let`、`const` 定义的变量，`只能在块作用域里访问`，`不能跨块访问`，`也不能跨函数访问`，`不存在变量提升`，`不可以重复声明`,如果有重复变量 let 会在编译阶段报错
- `let`、`const` 不会挂载到window属性上，var会

```js
// ES5问题?
// 在if或者for循环中声明的变量会变成全局变量
for (var i = 0; i <= 5; i++) {
  console.log("hello");
}
console.log(i); //5

```

## 暂时性死区

- 从一个`代码块`的开始直到`代码执行到声明变量的行之前`，let 或const 声明的变量都处于`暂时性死区`(Temporal dead zone，TDZ)中
- 当变量处于`暂时性死区之中`时，其尚未被初始化，尝试访问变量将抛出 ReferenceError。当代码执行到声明变量所在的行时，变量被初始化为一个值。如果声明中未指定初始值，则变量将被初始化为 undefined
-  var声明的变量不同，如果在声明前访问了变量，交量将会返回 undefined 。

```js
console.log(a) // undefined
var a;

// Uncaught ReferenceError: b is not defined
console.log(b) // 暂时性死区(Temporal dead zone，TDZ)中
let b;
```
