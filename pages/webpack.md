# webpack

## webpack介绍

- webpack是一个JavaScript 应用程序的静态模块打包工具

## 入口(entry)

- 入口起点(entry point)指示 webpack 应该使用哪个模块，来作为构建其内部 依赖图(dependency graph) 的开始。进入入口起点后，webpack 会找出有哪些模块和库是入口起点（直接和间接）依赖的

## 输出(output)

- output 属性告诉 webpack 在哪里输出它所创建的 bundle，以及如何命名这些文件


## loader

- webpack 只能理解 JavaScript 和 JSON 文件
- loader 让 webpack 能够去处理其他类型的文件，并将它们转换为有效模块，以供应用程序使用，以及被添加到依赖图中
- 多个loader，回从右往左执行

## 插件(plugin)

- loader 用于转换某些类型的模块，而插件则可以用于执行范围更广的任务。包括：打包优化，资源管理，注入环境变量

## webpack 编译流程

- 1、初始化参数：从配置文件和 Shell 语句中读取参数并且和webpack`参数进行合并`,得出最终的配置对象
- 2、用上一步得到的参数`初始化 Compiler `对象
- 3、加载所有的`plugin`配置的插件，调用插件的`apply`函数并且传递Compiler 对象
- 4、执行Compiler对象的 run 方法开始执行编译
- 5、根据配置中的entry找出入口文件
- 6、从入口文件出发,调用所有配置的`Loader`对模块进行编译
- 7、再找出该模块依赖的模块，再`递归`直到所有入口依赖的文件都经过了本步骤的处理
- 8、根据入口和`模块之间的依赖关系`，组装成一个个包含`多个模块的 Chunk`
- 9、再把每个 `Chunk 转换成一个单独的文件`加入到输出列表
- 10、在确定好输出内容后，根据配置确定输出的路径和文件名，把文件内容写入到文件系统


## module、chunk、bundlle

- module：各个源码文件，webpack 中一切皆模块
- chunk：多模块合并成的，如entry importo splitChunk
- bundlle：最终的输出文件