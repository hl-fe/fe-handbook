# vuex4.0

- <a href='https://gitee.com/hl-fe/fe-handbook/tree/master/code/vuex4.0' target=_blank>源码实现地址</a> 


## Vuex

- Vuex 是 Vue.js 中的状态管理工具，提供了一种集中式存储管理应用程序中所有组件的状态的方法。其目的是实现组件之间的状态共享、快速响应数据变化以及对复杂多交互行为进行封装和解耦。

## 在 Vuex 中，核心概念包括 state、getters、mutations、actions、module(多模块)

- state:为单一状态树，即将需要管理的多个组件的状态单独抽离出来，生成一个全局的单一状态集合
- getter 为类似于 computed 属性机制，可以监听 state 的变化来返回计算值
- mutations: 为修改状态值的函数，接收的参数为 state 对象
- action: 为异步操作，触发 mutation 来更新状态
- module: 对于大型项目，可以使用 module 将处理功能划分到不同的模块中来维护和管理

## vuex 执行流程
- 1、当在`Vue.use`安装`Store`时，会调用`createStore`函数，创建`Store`实例对象，Store内部会通过`provide`注入一个`store`实例在根组件上
- 2、将外部传递的`state`,`getters`,`mutations`,`actions`函数进行分类，存储在Store实例中
- 3、将格式化后的数据，进行递归模块安装，把所有的 state 状态 挂载到根 state 上，并且通过命名空间将 getter、mutations、action 进行分类，存储在 store 实例上
- 4、最后将根 state 的状态通过 reactive 转换成响应式数据
- 5、在调用 dispatch、commit 将收集的 mutations、action 统一进行派发
- 6、用户在使用的时候，调用vuex提供的`useStore`函数，内部通过`inject`去根组件查找`Store`实例，调用对应的模块

## mutations 为什么不能异步

- Vuex 中所有的状态更新的唯一途径都是 mutation，异步操作通过 Action 来提交 mutation 实现，这样使得我们可以方便地跟踪每一个状态的变化，从而让我们能够实现一些工具帮助我们更好地了解我们的应用。

- 每个 mutation 执行完成后都会对应到一个新的状态变更，这样 devtools 就可以打个快照存下来，然后就可以实现 time-travel 了。如果 mutation 支持异步操作，就没有办法知道状态是何时更新的，无法很好的进行状态的追踪，给调试带来困难。

## actions、mutations 区别
- actions中支持异步处理，mutations只能处理同步
- actions中可以提交多个mutations


## 为什么要store.commit('xx')才能触发事件执行？而不使用mutation函数进行操作

- 在store类中根本没有实现`mutation`方法，只能通过调用`commit`方法来执行`mutation`里的函数列表。

## 为什么不可以直接对state存储的状态进行修改，只能通过调用函数的方式修改呢？

- Vuex 通过强制限制对 store 的修改方式来确保状态的可追踪性。只有通过 mutation 函数才能修改 store 中的状态，这样可以轻松地跟踪状态的变化，也可以避免无意中从不同的组件中直接修改 store 导致的代码难以维护和调试的问题。

## 为什么存在异步调用的函数需要store.dispatch('xx')中完成呢？而不可以通过store.commit('xx')嘛？

- 因为在store中，在调用store.commit都是以同步的方式去调用`mutations`里的函数列表,本身就不支持异步
- 在store.dispatch中，是通过`Promise.all`的方式去调用的`actions`里的函数列表，支持异步