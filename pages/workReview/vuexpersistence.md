# vuex 数据持久化

## 痛点

- 用户的登录信息，一般情况下会存储在 vuex 中，可以实现跨组件通信
- 唯一的缺点就是，当用户在刷新页面的时候，vuex 中数据会全部初始化，导致登录信息丢失

## 设想

- 最开始的设想是每次在触发 mutations 的时候，手动的存储一份到 Storage，这个虽然可以实现但是需要人为的去干预，不太智能，就放弃了这个方案。利用 vuex 自定义 plugins 来实现

## 思路

- 1、vuex 插件机制是发布、订阅模式，在初始化时会执行plugins调用plugins中对函数，同时会传入 store 实例
- 2、当插件拿到 store 时，调用store实例的 subscribe 进行事件订阅
- 3、当通过 mutations 改变数据时，会触发subscribe订阅器执行，并且会传递 store 数据，这时会可以把store 数据存储在Storage
- 4、当页面被刷新时，vuex 插件会初始化，如果 Storage 中有数据，就执行 store.replaceState 替换 store 数据

```js
function statePersistence ( store ) {
  const localState = sessionStorage.getItem( 'VUEX_STATE' );
  if ( localState ) {
    // store.replaceState( JSON.parse( localState ) );
    store.replaceState({ ...toRaw(store.state), ...JSON.parse(localState) })

  }
  store.subscribe( ( { type }, state ) => {
    sessionStorage.setItem( 'VUEX_STATE', JSON.stringify( state ) );
  } )
}

const store = createStore( {
  plugins: [ statePersistence ]}
  ....
)
```
## 问题与思考

- 问题？
  - 同事加了一个新的store模块，直接刷新页面时，取新模块State数据报错，找不到该模块

- 原因  
  - 我们自己写了一个vuex持久化插件，当页面刷新的时候，我们会把持久化的数据，去替换Root.State
  - 由于是新的模块，此时又没有新的`mutations`提交，导致持久化的时候没有存储新的模块，直接取值会导致找不到该模块
  
- 思考
  - 页面在刷新的时候，获取到新的State，和持久化的State数据，进行合并

```js
// ...
store.replaceState({ ...toRaw(store.state), ...JSON.parse(localState) })
// ...
```