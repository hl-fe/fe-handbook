# stripe支付体检优化

## 业务痛点

- 三方支付采用的是stripe，支付时需要在Stripe页面完成支付，支付完成后用户再次回到当前页面，会导致当前页面的刷新，用户体验不好，甚至会中断用户的操作流程
- 利用BroadcastChannel跨页面通信解决了这个问题，满足了用户的支付需求，同时当前的操作页面不会刷新，用户体验更好


## 思路

:::tip
BroadcastChannel同源策略下，可以浏览器跨页面通信
:::

- 1、通过`window.open('xxx', '_blank')`,打开一个新的窗口方式，打开stripe支付链接
- 2、当前页面创建一个`BroadcastChannel`,监听`Message`消息
- 3、当支付完成后，stripe回跳指定的中间结果页面，通过`BroadcastChannel`,发送`postMessage`消息,并且通过` window.close()`关闭新窗口
- 4、用户还是停留在当前的页面，并且页面没有刷新

```js
// a页面
window.open('xxx', '_blank')

const channel = new BroadcastChannel('openask-deposite')
channel.addEventListener('message', (event) => {
  if (event.data === 'purchase-success') {
    onSuccess()
  }
})

// b页面
const channel = new BroadcastChannel('openask-deposite')
channel.postMessage('purchase-success')

setTimeout(() => {
  window.close()
}, 100)

```

## BroadcastChannel和postMessage区别

- 相同点：都可以实现跨页面通信
- 不同点：
  - postMessage不受跨域的限制，点对点传递`一对一`
  - BroadcastChannel必须同源策略下，类似广播传递`一对多`
- 适用场景：postMessage更加适合页面有父子关系的传输，比如内嵌入一个`iframe`