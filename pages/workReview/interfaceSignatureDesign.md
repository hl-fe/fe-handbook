# 接口签名设计

## 前言

- 主要是为了防止，其他人利用某一种工具恶意刷平台接口，造成数据库资源浪费、同时也可以一定程度的保证接口安全性

## 签名参数

| 字段      | 类型   | 必传 | 说明                |
| --------- | ------ | ---- | ------------------- |
| appkey    | string | true | 平台的生成的 appKey |
| timestamp | string | true | 时间戳毫秒          |
| noncestr  | string | true | 8 位随机字符        |
| signature | string | true | 生成的签名          |

## 签名设计方案

![eventLoop](/images/interfaceSignatureDesign.png)

- 1.页面首次打开时，首先获取服务端当前的时间
- 2.用客户端当前的时间`减去`获取服务端当前的时间,计算时间偏差(客户端时间-服务端时间)`通过Storage存储在本地`
- 3.在请求接口的时候，在请求头中，携带上参与签名的数据和签名
    - 1、再次获取客户端当前的时间`减去`计算时间偏差
    - 2、生成一个8位随机字符
    - 3、对接口根据ASCII码值的大小进行顺序，转成字符
    - 4、最后通过`appkey`、`timestamp`、`noncestr`、`paramsStr`进行MD5加密
- 4.服务端通过全局的拦截器进行拦截，解析请求头进行签名验证，服务端用`当前的时间戳`减去客户端传递的时间戳`取绝对值`，如果时间在范围内，通过客户端的参数，通过同样的方式进行 MD5(加密)，和客户端的进行对比

```js
import { stringify } from "qs";
import { Md5 } from "ts-md5";

const appkey = "BC001CMEA007"; // 平台的Key

const alphabeticalSort = (a, b) => {
  try {
    return a.localeCompare(b);
  } catch (e) {
    return a > b;
  }
};

function sign(params) {
  const offset = Number(sessionStorage.getItem("offset") || 0); // 客服端和服务器的时间差值
  const timestamp = Date.now() - offset; // 抹平差值
  const noncestr = Math.random().toString(36).slice(2, 10); // 8位随机字符
  // 对接口入参数进行排序(规则需要和服务端保持一致)
  const paramsStr = stringify(params, {
    sort: alphabeticalSort,
    arrayFormat: "repeat",
    allowDots: true,
    encode: true,
  });
  const signature = MD5(appkey + timestamp + noncestr + paramsStr); // 签名
  return {
    timestamp,
    noncestr,
    signature,
  };
}
```
