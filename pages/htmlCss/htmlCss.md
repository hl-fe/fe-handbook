# htmlCss总结

## html 加载

![domtree](/images/domtree.png)


### 1、构建 DOM 树：

- 浏览器接收到 HTML 文件后，开始解析 HTML 标记并构建 DOM（文档对象模型）树。
- DOM 树表示了文档的结构，每个 HTML 元素对应一个节点。

### 2、构建 CSSOM 树：

- 浏览器解析 CSS 文件并构建 CSSOM（CSS 对象模型）树。
- CSSOM 树表示了 CSS 样式规则的层级关系和继承关系。

### 3、合并 DOM 树和 CSSOM 树，生成 Render 树：

- 浏览器将 DOM 树和 CSSOM 树合并，生成 Render 树（渲染树）。
- Render 树只包含需要显示的节点和与之关联的样式信息，不包含隐藏的节点和样式。

### 4、布局（Layout）：

- 浏览器根据 Render 树计算每个节点在页面中的几何信息（位置、大小），形成布局。
- 计算过程中考虑了 CSS 盒模型、浮动、定位等属性。

### 5、绘制（Painting）：

- 浏览器使用计算好的几何信息绘制页面内容到屏幕上。
- 绘制过程包括文字渲染、图像绘制、背景绘制等操作。

### 6、合成与显示：

- 浏览器将绘制好的页面内容进行合成，形成最终的页面显示。
- 合成过程利用硬件加速和图形处理单元（GPU）来加速，提高渲染性能和流畅度。
- `transform`、`opacity`等一系特定的属性会触发硬件加速。

### 7、JavaScript 解析与执行：

- 如果 HTML 中包含了 JavaScript 脚本（script 标签），浏览器会解析并执行这些脚本。
- 脚本执行过程中可能会修改 DOM 树、CSSOM 树或触发页面的重新布局和重绘。

### 8、网络请求与资源加载：

- 在以上渲染步骤中，浏览器可能会发起额外的网络请求以加载页面所需的外部资源，如 CSS 文件、JavaScript 文件、图像等。


## CSS对DOM的解析、渲染

- 首先，DOM 和 CSSOM 通常是并行构建的，所以 CSS 加载不会阻塞 DOM 的解析。
- 由于 Render Tree 是依赖于 DOM Tree 和 CSSOM Tree 的，所以他必须等待到 CSSOM Tree 构建完成，也就是 CSS 资源加载完成(或者 CSS 资源加载失败)后，才能开始渲染。因此，CSS 加载会阻塞 Dom 的渲染。


## display:none和visibility:hidden的区别

- display: none元素消失，`不占位`，会触发reflow`（回流/重排）`，进行渲染。
- visibility: hidden; 元素消失，`占位`，只会触发`repaint（重绘）`，因为没有发现位置变化，不进行渲染。

## 盒模型

- 页面渲染时，dom 元素所采用的布局模型。可通过 box-sizing 进行设置。根据计算宽高的区域可分为：

  - content-box (W3C 标准盒模型)
  - border-box (IE 盒模型)
  - padding-box
  - margin-box

- css3 中 box-sizing 的取值及各值的说明
  content-box W3C 标准盒模型 Width/Height = border+padding+width/height
  border-box Width/Height =width /height-border-padding
  inherit 继承父元素的盒模型模式。

## 有三个元素在一排，高度不固定，需要三个等高

- 直接给父元素:display: flex,子元素高度不要固定,随着内容的高度变化;

## 居中布局

- 水平居中

  - 行内元素: text-align:center
  - 块级元素: margin:0 auto
  - absolute+transform
  - flex+justify-content:center

- 垂直居中

  - line-height:height
  - absolute+transform
  - flex+align-items:center
  - table

- 水平垂直居中
  - absolute+transform
  - flex+justify-content+align-items

## 选择器优先级

- important > 行内样式 > #id > .class > tag > > 继承 > 默认
- 选择器 从右往左 解析

## 行内元素

- span a b i input img strong em br textarea button 伪类 before after

## 块级内元素

- div ul li ol h1-h6 p table
- 行内元素无法设宽高，有 padding 上下左右有效，margin 上下无效，左右有效 display:inline

### HTML5 新增

- header、section、aside、footer input 属性
- 级元素可以设宽高，padding margin 上下左右都有效，自动盛满一整行

## 什么是 BFC，作用有哪些？哪些情况下会触发 BFC？

- 什么是 BFC，作用有哪些？哪些情况下会触发 BFC？
- BFC（块级格式化上下文），是页面上的一个隔离的独立容器，容器里面的子元素不会影响到外面元素，反之亦然。它与普通的块框类似，但不同之处在于
  - 可以阻止元素被浮动元素覆盖。
  - 可以包含浮动元素。
  - 可以阻止 margin 重叠。
  
- 满足下列条件之一就可触发 BFC：

  - 根元素，即 HTML 元素
  - float 的值不为 none
  - overflow 的值不为 visible
  - display 的值为 inline-block、table-cell、table-caption
  - position 的值为 absolute 或 fixed

## 清除浮动的几种方式，各自的优缺点

- 1.使用空标签清除浮动 clear:both。

  - 原理：添加一个空 div，利用 css 提高的 clear:both 清除浮动，让父级 div 能自动获取到高度
  - 优点：通俗易懂，容易掌握
  - 缺点：会添加很多无意义的空标签，有违结构与表现的分离，在后期维护中将是噩梦

- 2.父级 div 定义 overflow:hidden

  - 原理：必须定义 width 或 zoom:1，同时不能定义 height，使用 overflow:hidden 时，浏览器会自动检查浮动区域的高度
  - 优点：简单，代码少，浏览器支持好
  - 缺点：不能和 position 配合使用，因为超出的尺寸的会被隐藏

- 3.父级 div 定义伪类:after 和 zoom(用于非 IE 浏览器)

  - 原理：IE8 以上和非 IE 浏览器才支持:after，原理和方法 1 有点类似，zoom(IE 转有属性)可解决 ie6,ie7 浮动问题
  - 优点：浏览器支持好，不容易出现怪问题（目前：大型网站都有使用，如：腾迅，网易，新浪等等）。
  - 缺点：代码多，要两句代码结合使用，才能让主流浏览器都支持

- 4、父级 div 定义 height

  - 原理：父级 div 手动定义 height，就解决了父级 div 无法自动获取到高度的问题。
  - 优点：简单，代码少，容易掌握
  - 缺点：只适合高度固定的布局，要给出精确的高度，如果高度和父级 div 不一样时，会产生问题

- 父级 div 定义 overflow:auto、
  - 原理：必须定义 width 或 zoom:1，同时不能定义 height，使用 overflow:auto 时，浏览器会自动检查浮动区域的高度
  - 优点：简单，代码少，浏览器支持好
  - 缺点：内部宽高超过父级 div 时，会出现滚动条。

## 自定义 checkbox

```css
input[type="checkbox"] {
  width: 13px;
  height: 13px;
  display: inline-block;
  text-align: center;
  vertical-align: middle;
  line-height: 13px;
  top: -3px;
  position: relative;
}

input[type="checkbox"]::before {
  content: "";
  position: absolute;
  top: 0;
  left: 0;
  background: #fff;
  width: 100%;
  height: 100%;
  border-radius: 3px;
  border: 1px solid #d9d9d9;
}

input[type="checkbox"]:checked::before {
  content: "\2713";
  background-color: #fff;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  border: 1px solid #e50232;
  color: #e50232;
  font-size: 13px;
  font-weight: bold;
}
```
