# bfc

## 何为BFC

- MDN给出的解释是:`块级格式化上下文`，指一个独立的渲染区域，有着自己的`渲染规则`，其内部的元素不会和外部的`互相影响`

## 触发BFC的条件

- 1、浮动元素，float `除 none` 以外的值； 
- 2、定位元素，position（`absolute`，`fixed`）； 
- 3、display 为以下其中之一的值 `inline-block`，`table-cell`，`table-caption`； 
- 4、overflow 除了 `visible` 以外的值（hidden，auto，scroll）； 

## BFC的特性

- 1.内部的Box会在垂直方向上一个接一个的放置。
- 2.垂直方向上的距离由margin决定
- 3.bfc的区域不会与float的元素区域重叠。
- 4.计算bfc的高度时，浮动元素也参与计算
- 5.bfc就是页面上的一个独立容器，容器里面的子元素不会影响外面元素。

## BFC的解决的问题之一

- 如果二个div元素上下排列，一个设置margin-bottom，另外一个设置margin-top，会导致margin上下重叠，取比较大的一个值

:::demo

bfc

:::