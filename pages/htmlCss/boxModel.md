# 盒模型

## 盒模型计算
- box=边框+内边距离+content
- box-sizing: border-box 让盒子宽高等于设置的

::: tip
如下设置了box-sizing: border-box; box=100px<br/>
如果未设置box=100+10+10+2=122px
:::
```html
<style type="text/css">
  #box {
    width: 100px;
    padding: 10px;
    border: 1px solid #ccc;
    margin: 10px;
    box-sizing: border-box;
  }
</style>
<div id="box">
    this is div1
</div>
```