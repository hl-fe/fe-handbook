# margin(可以通过触发BFC的方式解决)

## margin-上下重叠

:::warning
2个相邻的元素margin-bottom/top，会重叠,已距离大的为准
:::

:::tip
margin-left,right 不会出现这个问题
:::

:::demo Use `2个元素上下相差50px`
margin-01
:::

## margin-负值

:::demo Use `margin-top负数，元素本身上移动`
margin-02
:::

:::demo Use `margin-bottom负数，元素本身不动，下方元素上移动`
margin-03
:::

:::demo Use `margin-left负数，元素本身左移动`
margin-04
:::

:::demo Use `margin-right负数，元素本身不动，右方元素左移动`
margin-05
:::

