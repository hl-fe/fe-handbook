# rem-em-vw

:::tip
移动端响应式布局em、rem、vw都是相对布局
:::

:::demo em:相对于父元素,如果父元素的 `font-size: 50px`，那么子元素`1em=50px`
em
:::

:::demo em:相对于html元素,如果htm元素的 `font-size: 100px`，那么子元素`1rem=100px`
rem
:::

:::demo vw:视口会被均分为`100单位的vw`，如果适口是750，750/100=7.5，`1vw就等于7.5px`
vw
:::