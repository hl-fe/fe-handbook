# grid 布局

## 概述

网格布局（Grid）是最强大的 CSS 布局方案。它将网页划分成一个个网格，可以任意组合不同的网格，做出各种各样的布局。以前，只能通过复杂的 CSS 框架达到的效果，现在浏览器内置了。

## grid 网格分为行`rows`和列`columns`

- 行 `rows` 设置的是高度 
- 列 `columns` 设置的是宽度 

## 定义网格及 fr 单位均分

:::demo

grid/000

:::

## 合并网格及网格命名`grid-template-areas`

:::demo

grid/001

:::

## 网格间隙及简写

:::demo

grid/002

:::

## 网格对齐方式及简写

:::demo

grid/003

:::

## 显式网格与隐式网格

:::demo `如果定义的是三行两列 3*2=6，超出的就是隐式网格`

grid/004

:::

## 基于线的元素放置

:::demo

grid/005

:::

## 基于线的命名元素放置

:::demo

grid/006

:::

## 基于线的 span，行/列占用网格

:::demo

grid/007

:::

## repeat()

:::demo

grid/008

:::

## minmax()

:::demo

grid/009

:::

## repeat() minmax()组合自适应布局

:::demo`auto-fill、auto-fit容器不够自动换行`

grid/010

:::

## 网格模拟定位

:::demo

grid/011

:::

## span\grid-area 组合写法

:::demo

grid/012

:::

## 栅格系统

:::demo

grid/013

:::

## grid 布局练习一

:::demo

grid/014

:::

## grid 布局练习二(防小米商城菜单)

:::demo

grid/015

:::

