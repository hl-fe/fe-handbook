# nginx

## nginx 正向代理
- 代理的用户请求，和用户请求方向一致，叫做正向代理。

![正向代理](/images/nginx1.png)

## nginx 反向代理
- 代理是代理服务器处理用户请求，和用户请求方向相反，叫做反向代理。
![反向代理](/images/nginx1.png)

## nginx 全局设置优化

- worker_processes 用来设置 Nginx 服务的进程数。推荐是 CPU 内核数或者内核数的倍数，推荐使用 CPU 内核数
  > worker_processes 4;

- 默认情况下，Nginx 的多个进程有可能跑在某一个 CPU 或 CPU 的某一核上，导致 Nginx 进程使用硬件的资源不均
  > worker_cpu_affinity 0001 0010 0100 1000;

- 设置一个进程理论允许的最大连接数，理论上越大越好，但不可以超过 worker_rlimit_nofile 的值
  > worker_rlimit_nofile 200000;

## 查看服务器最大可以打开文件的数量

```js
ulimit -a 查看

ulimit -n 300000  更改
```

## 升级 http2

- 1、nginx 的版本必须在 1.9.5 以上
- 2、openssl 的版本必须在 1.0.2e 及以上

```js
server {
listen 443 ssl http2;
}
```

## nginx 模块

```js
#nginx进程数，建议设置为等于CPU总核心数
worker_processes  1;

#全局错误日志定义类型，[ debug | info | notice | warn | error | crit ]
#error_log  logs/error.log;

# 一个nginx进程打开的最多文件
worker_rlimit_nofile 65535;

#工作模式与连接数上限
events {
   # 单个进程最大连接数（最大连接数=连接数*进程数）该值受系统进程最大打开文件数限制，需要使用命令ulimit -n 查看当前设置
   worker_connections 65535;
}

#设定http服务器
http {

    #长连接超时时间，单位是秒，默认为0
    keepalive_timeout  65;

    # gzip压缩功能设置
    gzip on; #开启gzip压缩输出
    gzip_comp_level 6; #压缩等级
    gzip_types application/javascript # 类型不配置不生效




    # http_proxy服务全局设置
    client_max_body_size   10m;


   # 设定负载均衡后台服务器列表
    upstream  backend.com  {
        server   192.168.10.100:8080 max_fails=2 fail_timeout=30s ;
        server   192.168.10.101:8080 max_fails=2 fail_timeout=30s ;
    }

    #虚拟主机的配置
    server {
        #监听端口
        listen       80;
        #域名可以有多个，用空格隔开
        server_name  localhost fontend.com;

        #代理
        location / {
          proxy_pass http://127.0.0.1:3000;
      }

       # 图片缓存时间设置
       location ~ .*.(gif|jpg|jpeg|png|bmp|swf)$ {
          expires 10d;
       }

       # JS和CSS缓存时间设置
       location ~ .*.(js|css)?$ {
          expires 1h;
       }
}

```


## CORS 分类(简单请求和非简单请求)

### 简单请求

 - 1、请求方法是以下三种方法之一:`HEAD` `GET` `POST`
 - 2、HTTP的头信息不超出以下几种字段:
    > Accept

    > Accept-Language

    > Content-Language

    > Last-Event-ID

    > Content-Type：只限于三个值a`pplication/x-www-form-urlencoded`、`multipart/form-data`、`text/plain`


### 非简单请求

- 非简单请求是那种对服务器有特殊要求的请求，比如请求方法是PUT、DELETE或OPTIONS，或者Content-Type字段的类型是application/json。

- 对于非简单 CORS 请求会有一个预检机制，预捡的请求方法是 OPTIONS，如果不做处理状态码会返回 403 或者 405，我们需要在 nginx 对 OPTIONS 方法进行处理让状态码返回 200

## CORS 配置

```js
location / {
   add_header 'Access-Control-Allow-Origin' '*';// 跨域白名单
   add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, DELETE';
   add_header 'Access-Control-Allow-Headers' 'Content-Type';
   # 处理非简单请求
   if ($request_method = 'OPTIONS') {
      return 200;
   }
}

```

## nginx 基础配置

```js
user  nginx;
worker_processes  4;



include /usr/share/nginx/modules/mod-stream.conf;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;
    server_tokens       off;
    proxy_intercept_errors on; 

   #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
   #                  '$status $body_bytes_sent "$http_referer" '
   #                  '"$http_user_agent" "$http_x_forwarded_for"';

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for" '
                      '"$http_host" "$upstream_addr" "$upstream_status" "$upstream_response_time" "$request_time"';

    access_log  /var/log/nginx/access.log  main;
    client_max_body_size 100m;
    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

   #large_client_header_buffers  4 1m;

    include /etc/nginx/conf.d/*.conf;
    limit_req_zone $binary_remote_addr zone=myRateLimit:10m rate=10r/s;
}

stream {

    log_format  basic   '$time_iso8601 $remote_addr '
                        '$protocol $status $bytes_sent $bytes_received '
                        '$session_time $upstream_addr '
                        '"$upstream_bytes_sent" "$upstream_bytes_received" "$upstream_connect_time"';

    access_log      /var/log/nginx/stream.log  basic buffer=1k flush=5s;

    include /etc/nginx/conf.d/*.tcp;

}

```

## 静态资料配置

```js
server {
        listen       50080;
        server_name  localhost;

		#charset koi8-r;
		#access_log  logs/host.access.log;
        
		
        location /internet_payment_account {
                alias  /usr/share/nginx/html/frontapp/internet_payment_account/dist;
                index  index.html index.htm;
                try_files $uri $uri/ /internet_payment_account/index.html;
        }
        
        location /ipa-bizcenter/api {
                client_max_body_size 50m;
                proxy_pass http://10.123.78.82:8001/ipa-bizcenter;
        }                      
}

```